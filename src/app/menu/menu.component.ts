import { Component, OnInit } from '@angular/core';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  constructor(private cookieService: CookieService) {  }

  public static reloadLogIntext(text) {
    document.getElementById('logInButton').innerText = text;
  }
  public  static reloadSignUpText(text) {
    document.getElementById('signUpButton').innerText = text;
  }

  ngOnInit() {
    const user = this.cookieService.get('userLoged');
    if (user !== '' || user == null) {
      document.getElementById('logInButton').innerText = user;
      document.getElementById('signUpButton').innerText = '';
    } else {
      document.getElementById('logInButton').innerText = 'LogIn';
      document.getElementById('signUpButton').innerText = 'SignUp';
    }
  }

}
