import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import ApiUtils from '../utils/ApiUtils';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  // public manager: SteviaManager;
  public formSignUp: FormGroup;
  public mostrarError = false;
  public mensajeError: string [] = [];
  constructor() {
    this.formSignUp = new FormGroup(
      {
        name: new FormControl('', [Validators.required, Validators.minLength(3)]),
        mail: new FormControl('', [Validators.required, Validators.minLength(5)]),
        rmail: new FormControl('', [Validators.required, Validators.minLength(5)]),
        pass: new FormControl('', [Validators.required, Validators.minLength(5)]),
        rpass: new FormControl('', [Validators.required, Validators.minLength(5)])
      }
    );
  }

  ngOnInit() {
  }

  async onSubmit() {
    const api = new ApiUtils('users');
    const rmail = this.formSignUp.controls.rmail.value;
    const mail = this.formSignUp.controls.mail.value;
    const pass = CryptoJS.MD5(this.formSignUp.controls.pass.value);
    const rpass = CryptoJS.MD5(this.formSignUp.controls.rpass.value);
    const name = this.formSignUp.controls.name.value;
    if ((mail === rmail) && (pass.toString() === rpass.toString())) {
      const res = await api.signUpRequest(name, mail, pass);
    } else {
      alert('Mails are different');
    }
    alert('Sign up correct, look up your mail');
    this.limpiaForm();
  }

  limpiaForm() {
    this.mensajeError = [];
    this.mostrarError = false;
    this.formSignUp.get('name').setValue('');
    this.formSignUp.get('pass').setValue('');
    this.formSignUp.get('rpass').setValue('');
    this.formSignUp.get('rmail').setValue('');
    this.formSignUp.get('mail').setValue('');
  }
}


// TODO: signUP rest request
