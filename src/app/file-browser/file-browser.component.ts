import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import ApiUtils from '../utils/ApiUtils';
import {CookieService} from 'ngx-cookie-service';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {FileUploader} from 'ng2-file-upload';
import axios, {} from 'axios';

@Component({
  selector: 'app-file-browser',
  templateUrl: './file-browser.component.html',
  styleUrls: ['./file-browser.component.css']
})
export class FileBrowserComponent implements OnInit {
  user: string;
  files;
  backFolder;
  currentFolder: string;
  pathBar;
  item: any;
  public uploader: FileUploader = new FileUploader({url: 'http://lab01:8080/MetaApi/files/ngfileupload'});
  public hasBaseDropZoneOver = false;
  public hasAnotherDropZoneOver = false;
  @Output() selectedFile = new EventEmitter<string>();
  @Input() fileType: string;

  constructor(public ngxSmartModalService: NgxSmartModalService, private cookieService: CookieService) {
  }

  ngOnInit() {
    this.user = this.cookieService.get('userLoged');
    this.backFolder = [];
    this.currentFolder = 'files';
    this.pathBar = document.getElementById('filesPath');
    this.pathBar.innerText = this.currentFolder + '/';
    const backButton = document.getElementById('backButton');
    const createFolderButton = document.getElementById('addFolderButton');
    backButton.addEventListener('click', (e: Event) => this.goBack());
    createFolderButton.addEventListener('click', (e: Event) => this.createFolder());
    this.loadUserFiles();
  }

  async loadUserFiles() {
    const api = new ApiUtils('files');
    const resp = await api.getFilesFromUser(this.user);
    if (resp.length > 0) {
      this.files = JSON.parse(resp);
    }
    this.createTable(this.files);
  }


  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }

  createTable(files: Array<string>) {
    this.cleanTable();
    files = files.sort(function compareTypes(a, b) {
      // @ts-ignore
      if (a.type.match('FOLDER')) {
        return 1;
      }
      // @ts-ignore
      if (b.type.match('FOLDER')) {
        return -1;
      }
    });

    if (files.length > 0) {
      const folders = [];
      const regFiles = [];
      const numTerms = files.length;
      const table = document.getElementById('fileManager') as HTMLDivElement;
      let row = '';
      let name = '';
      let allRows = '';
      let splitedPath = [];
      for (let i = 0; i < numTerms; i++) {
        // @ts-ignore
        splitedPath = files[i].path.split('/');
        const fatherFolder = splitedPath[splitedPath.length - 2];
        // @ts-ignore
        if (fatherFolder.match(this.currentFolder.split('/').pop())) {
          // @ts-ignore
          name = files[i].name;
          if ((name.length) > 15) {
            // @ts-ignore
            name = files[i].name.substring(0, 12) + '...';
          }
          row = '<div class="col s2 fileManagerItem center">';
          // @ts-ignore
          if (files[i].type.match('FOLDER')) {
            row += '<p><i name=folderButtonShow class="material-icons large center" style="cursor: default; \
                      color: #26a69a;">folder_open</i></p>';
            row += '<label>\n' +
              '      <span style="color: black">' + name + '</span>\n' +
              '    </label>';
            folders.push(files[i]);
          } else {
            // @ts-ignore
            if (files[i].bioFormat.match('expressionMatrix')) {
              row += '<i name=filesButtonShow class="material-icons large center" style="cursor: default; margin-top: 1vh;"><img\n' +
                '            src="../../assets/images/csv.png" height="100" width="100"/></i><p></p>';
              row += '<label>\n' +
                '     <span style="color: black">' + name + '</span>\n' +
                '    </label>';
              regFiles.push(files[i]);
            } else {
              row += '<i name=filesButtonShow class="material-icons large center" style="cursor: default;  margin-top: 2vh;"><img\n' +
                '            src="../../assets/images/ddf.png" height="80" width="70"/></i><p></p>';
              row += '<label>\n' +
                '      <span style="color: black">' + name + '</span>\n' +
                '    </label>';
              regFiles.push(files[i]);
            }

          }
          row += '</div>';
          allRows += row;
        }
      }
      table.innerHTML += allRows;
      let b = document.getElementsByName('folderButtonShow');
      for (let i = 0; i < b.length; i++) {
        // @ts-ignore
        b[i].addEventListener('dblclick', (e: Event) => this.openFolder(folders[i].name, true));
      }
      b = document.getElementsByName('filesButtonShow');
      for (let i = 0; i < b.length; i++) {
        // @ts-ignore
        b[i].addEventListener('dblclick', (e: Event) => this.filesShowButton(regFiles[i]));
      }
    }
  }

  async createFolder() {
    const elem = document.getElementById('folderName') as HTMLInputElement;
    const api = new ApiUtils('files');
    const folderName = this.currentFolder + '/' + elem.value;
    const res = await api.createFolder(this.user, folderName, this.currentFolder);
    elem.value = '';
    await this.loadUserFiles();
  }

  cleanTable() {
    const table = document.getElementById('fileManager') as HTMLDivElement;
    table.innerHTML = '';
  }

  async openFolder(folder: string, logChange: boolean) {
    if (folder.match('files') || folder.match('files/')) {
      this.loadUserFiles();
      this.backFolder = [];
      this.currentFolder = 'files';
      this.pathBar.innerText = 'files/';
    } else {
      if (logChange) {
        this.backFolder.push(this.currentFolder);
        this.currentFolder += '/' + folder;
      }
      this.pathBar.innerText = this.currentFolder;
      const api = new ApiUtils('files');
      const folderContent = await api.getFolderContent(this.user, this.currentFolder);
      let myParsed = [];
      if (folderContent.length > 0) {
        myParsed = JSON.parse(folderContent);
      }
      this.createTable(myParsed);
    }
  }

  async goBack() {
    if (this.backFolder.length > 0) {
      const item = this.backFolder.pop();
      this.currentFolder = item;
      const goTo = item.split('/').pop();
      await this.openFolder(goTo, false);
    }
  }

  filesShowButton(file) {
    this.pathBar.innerText = this.currentFolder + '/' + file.name;
    this.ngxSmartModalService.setModalData(file, 'infoModal');
    this.selectedFile.emit(file);
  }

  async uploadFiles(item: any) {
    const api = new ApiUtils('files');
    const a = document.getElementById('techInput' + item.file.name) as HTMLInputElement;
    console.log (a.value);
    const b = this.axiosUpload(item.file.rawFile, this.user, item.file.name, 'FILE', a.value, this.currentFolder, this.currentFolder);
    console.log (b);
    await this.loadUserFiles();
  }

  async axiosUpload(fileSend: File, user: string, nameFile: string, typeFile: string, tech: string, path: string, parent: string) {
    console.log (fileSend);
    const data = new FormData();
    data.append('file', fileSend);
    data.append('user', user);
    data.append('nameFile', nameFile);
    data.append('type', typeFile);
    data.append('tech', tech);
    data.append('path', path);
    data.append('parent', parent);
    console.log (data.get('path'));
    axios.post('http://localhost:8080/MetaApi/files/uploadWithPost', data, {
      onUploadProgress: (progressEvent) => {
        if (progressEvent.lengthComputable) {
          this.updateProgressBarValue(progressEvent, nameFile);
        }
      }
    }).then(
      response => {
        this.loadUserFiles();
      } // if the response is a JSON object // Handle the success response object
    ).catch(
      error => console.log(error) // Handle the error response object
    );
  }

  updateProgressBarValue(progressEvent: any, itemname: any) {
    const progress = progressEvent.loaded / progressEvent.total;
    document.getElementById('progressBar' + itemname).setAttribute('style', 'width: ' + progress * 100 + '%');
  }

  fileDroped($event: File[]) {
    console.log ($event);
  }
}
