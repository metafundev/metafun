import { Component, OnInit } from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';
import ApiUtils from '../utils/ApiUtils';
import {NgxSmartModalService} from 'ngx-smart-modal';


@Component({
  selector: 'app-data-page',
  templateUrl: './data-page.component.html',
  styleUrls: ['./data-page.component.css']
})
export class DataPageComponent implements OnInit {
  private basePath;
  private user: string;
  private files;
  private backFolder;
  private selectedFile;
  private currentFolder: string;
  private pathBar;

  constructor(private cookieService: CookieService, public ngxSmartModalService: NgxSmartModalService) { }


  ngOnInit() {
    this.user = this.cookieService.get('userLoged');
    this.basePath = '/home/pmalmierca/.metafunusers/' + this.user + '/files/';
    this.backFolder = [];
    this.currentFolder = 'files';
    this.pathBar = document.getElementById('filesPath');
    this.pathBar.innerText = this.currentFolder + '/';
    const backButton = document.getElementById('backButton');
    const uploadButton = document.getElementById('uploadButton');
    const createFolderButton = document.getElementById('addFolderButton');
    const showUpload = document.getElementById('showUploadInfoButton');
    const showInfo = document.getElementById('showInfoButton');
    const deleteFileButton = document.getElementById('deleteFileButton');
    const renameFileButton = document.getElementById('renameFileButton');
    const renameActionButton = document.getElementById('renameActionButton');
    backButton.addEventListener('click', (e: Event) => this.goBack());
    uploadButton.addEventListener('click', (e: Event) => this.uploadFile());
    createFolderButton.addEventListener('click', (e: Event) => this.createFolder());
    showUpload.addEventListener('click', (e: Event) => this.showUploadFile());
    showInfo.addEventListener('click', (e: Event) => this.showFileInfo());
    deleteFileButton.addEventListener('click', (e: Event) => this.deleteFileButton());
    renameFileButton.addEventListener('click', (e: Event) => this.renameFileButton());
    renameActionButton.addEventListener('click', (e: Event) => this.renameActionButton());
    const uploadButton1 = document.querySelector('.browse-btn');
    const fileInfo = document.querySelector('.file-info');
    const realInput = document.getElementById('real-input');

    uploadButton1.addEventListener('click', (e) => {
      realInput.click();
    });

    realInput.addEventListener('change', () => {
      // @ts-ignore
      const name = realInput.value.split(/\\|\//).pop();
      const truncated = name.length > 25
        ? name.substr(name.length - 25)
        : name;

      fileInfo.innerHTML = truncated;
    });
    this.loadUserFiles();
  }

  async loadUserFiles() {
    const api = new ApiUtils('files');
    const resp = await api.getFilesFromUser(this.user);
    if (resp.length > 0) {
      this.files = JSON.parse(resp);
    } else {
      this.files = [];
    }
    this.createTable(this.files);
  }

  createTable(files: Array<string>) {

    this.cleanTable();
    files = files.sort(function compareTypes(a, b) {
      // @ts-ignore
      if (a.type.match('FOLDER')) {
        return -1;
      }
      // @ts-ignore
      if (b.type.match('FOLDER')) {
        return 1;
      }
    });

    if (files.length > 0) {
      const folders = [];
      const regFiles = [];
      const numTerms = files.length;
      const table = document.getElementById('fileManager') as HTMLDivElement;
      let row = '';
      let name = '';
      let allRows = '';
      const splitedPath = [];
      for (let i = 0; i < numTerms; i++) {
        // @ts-ignore
        splitedPath = files[i].path.split('/');
        const fatherFolder = splitedPath[splitedPath.length - 2];
        // @ts-ignore
        if (fatherFolder.match(this.currentFolder.split('/').pop())) {
          // @ts-ignore
          name = files[i].name;
          if ((name.length) > 15) {
            // @ts-ignore
            name = files[i].name.substring(0, 12) + '...';
          }
          row = '<div class="col s2 fileManagerItem center">';
          // @ts-ignore
          if (files[i].type.match('FOLDER')) {
            row += '<p><i name=folderButtonShow class="material-icons large center" style="cursor: default">folder_open</i></p>';
            row += '<label>\n' +
              '      <span style="color: black">' + name + '</span>\n' +
              '    </label>';
            folders.push(files[i]);
          } else {
            // @ts-ignore
            if (files[i].type.match('expressionMatrix')) {
              row += '<i name=filesButtonShow class="material-icons large center" style="cursor: default">assessment</i><p></p>';
              row += '<label>\n' +
                '     <span style="color: black">' + name + '</span>\n' +
                '    </label>';
              regFiles.push(files[i]);
            } else {
              row += '<i name=filesButtonShow class="material-icons large center" style="cursor: default">assignment</i><p></p>';
              row += '<label>\n' +
                '      <span style="color: black">' + name + '</span>\n' +
                '    </label>';
              regFiles.push(files[i]);
            }

          }
          row += '</div>';
          allRows += row;
        }
      }
      // this.addButtons();
      table.innerHTML += allRows;
      let b = document.getElementsByName('folderButtonShow');
      for (let i = 0; i < b.length; i++) {
        // @ts-ignore
        b[i].addEventListener('dblclick', (e: Event) => this.openFolder(folders[i].name, true));
      }
      b = document.getElementsByName('filesButtonShow');
      for (let i = 0; i < b.length; i++) {
        // @ts-ignore
        b[i].addEventListener('dblclick', (e: Event) => this.filesShowButton(regFiles[i]));
      }
    }
  }

  async createFolder() {
    const elem = document.getElementById('folderName') as HTMLInputElement;
    const api = new ApiUtils('files');
    const folderName = this.currentFolder + '/' + elem.value.replace(new RegExp(' ', 'g'), '_');
    const res = await api.createFolder(this.user, folderName, this.currentFolder);
    await this.loadUserFiles();
    console.log(res);
  }

  cleanTable() {
    const table = document.getElementById('fileManager') as HTMLDivElement;
    table.innerHTML = '<br>';
  }

  showUploadFile() {
    document.getElementById('tableDiv').setAttribute('class', 'hide');
    document.getElementById('uploadFilesDiv').setAttribute('class', 'row');
    document.getElementById('showUploadInfoButton').setAttribute('class', 'hide');
    document.getElementById('showInfoButton')
      .setAttribute('class', 'waves-effect btn center col s10');
  }

  showFileInfo() {
    document.getElementById('tableDiv').removeAttribute('class');
    document.getElementById('uploadFilesDiv').setAttribute('class', 'row hide');
    document.getElementById('showUploadInfoButton')
      .setAttribute('class', 'waves-effect btn center col s10');
    document.getElementById('showInfoButton').setAttribute('class', 'hide');
  }

  deleteFileButton() {
    const api = new ApiUtils('files');
    const nameFile = this.selectedFile.path.substring(this.basePath.length, this.selectedFile.path.length);
    api.deleteFile(this.user, nameFile);
    this.cleanTable();
    this.loadUserFiles();
  }

  renameFileButton() {
    const newNameButton = document.getElementById('renameFileButton');
    const newNameInput = document.getElementById('renameInput');
    const nameRow = document.getElementById('nameRow');
    const renameActionButton = document.getElementById('renameActionButton');
    if (!newNameButton.getAttribute('class').match('hide')) {
      newNameButton.setAttribute('class', 'hide');
      nameRow.setAttribute('class', 'hide');
      newNameInput.setAttribute('class', '');
      renameActionButton.setAttribute('class', 'col s5 btn waves-effect left');
    } else {
      newNameButton.setAttribute('class', 'col s5 btn waves-effect left');
      nameRow.setAttribute('class', '');
      newNameInput.setAttribute('class', 'hide');
      renameActionButton.setAttribute('class', 'hide');
    }
  }

  async renameActionButton() {
    const api = new ApiUtils('files');
    const splitedOldName = document.getElementById('filesPath').innerText.split('/');
    const newNameInput = document.getElementById('renameInput') as HTMLInputElement;
    let baseName = '';
    for (let i = 1; i < splitedOldName.length - 1; i++) {
      if (i < splitedOldName.length - 1) {
        baseName += splitedOldName[i] += '/';
      }
    }
    const oldName = baseName + splitedOldName[splitedOldName.length - 1];
    const newName = baseName + newNameInput.value;
    api.renameFile(this.user, oldName, newName);
    document.getElementById('nameRow').innerText = newNameInput.value;
    this.renameFileButton();
    this.loadUserFiles();
  }

  async uploadFile() {
    const radioFileType: NodeList = document.getElementsByName('fileTypeRadio');
    const techSelection = document.getElementById('techSelection') as HTMLSelectElement;
    let filecheck = -1;
    let fileType = '';
    let tech = 'NA';

    for (let i = 0; i < radioFileType.length ; i++) {
      // @ts-ignore
      if (radioFileType[i].checked) {
        filecheck = i;
      }
    }

    switch (filecheck) {
      case 0:
        fileType = 'expressionMatrix';
        if (techSelection.selectedIndex === 0) {
          tech = 'array';
        } else {
          tech = 'RNAseq';
        }
        break;
      case 1:
        fileType = 'dataDesign';
        break;
      case 2:
        fileType = 'anotFile';
        break;
      default:
        fileType = 'none';
    }

    if (!fileType.match('none')) {
      const filesElement = (document.getElementById('real-input') as HTMLInputElement);
      const api = new ApiUtils('files');
      const files = filesElement.files;
      if (files.length > 0) {
        const nameFile = files[0].name.replace(new RegExp(' ', 'g'), '_');
        // const resp = await api.uploadFile(files[0], this.cookieService.get('userLoged'), this.currentFolder + '/' + nameFile,
        //  fileType, tech);
        /*const resp = 404;
        if (resp === 200) {
          alert('File Uploaded Correctly');
        } else {
          alert('An error has ocurred, please try again later');
        }*/
      }
    } else {
      alert('An error has ocurred, please try again later.\n If the error persist, please contact us');
    }
    this.loadUserFiles();
    this.showFileInfo();
  }

  async openFolder(folder: string, logChange: boolean) {
    if (folder.match('files') || folder.match('files/')) {
      this.loadUserFiles();
      this.backFolder = [];
      this.currentFolder = 'files';
      this.pathBar.innerText = 'files/';
    } else {
      if (logChange) {
        this.backFolder.push(this.currentFolder);
        this.currentFolder += '/' + folder;
      }
      this.pathBar.innerText = this.currentFolder;
      const api = new ApiUtils('files');
      const folderContent = await api.getFolderContent(this.user, this.currentFolder);
      let myParsed = [];
      if (folderContent.length > 0) {
        myParsed = JSON.parse(folderContent);
      }
      this.createTable(myParsed);
    }

  }

  async goBack() {
    if (this.backFolder.length > 0) {
      const item = this.backFolder.pop();
      this.currentFolder = item;
      const goTo = item.split('/').pop();
      await this.openFolder(goTo, false);
    }
  }

  async filesShowButton(file) {
    const api = new ApiUtils('files');
    document.getElementById('tableDiv').setAttribute('class', '');
    document.getElementById('uploadFilesDiv').setAttribute('class', 'row hide');
    this.pathBar.innerText = this.currentFolder + '/' + file.name;
    const nameField = document.getElementById('nameRow');
    const typeField = document.getElementById('typeRow');
    const dateField = document.getElementById('dateRow');
    const sizeField = document.getElementById('sizeRow');
    const tagsField = document.getElementById('tagsRow');
    const tagsRow = document.getElementById('tagTR');
    let tags;
    nameField.innerText = '\t' + file.name;
    typeField.innerText = '\t' + file.type;
    dateField.innerText = '\t' + file.createdAt;
    const nSize = (+file.size * (1e-6)).toFixed(3);
    sizeField.innerText = '\t' + nSize + ' Mb';
    if (file.type.match('dataDesign')) {
      tagsRow.setAttribute('class', '');
      tags = await api.getExperimentalDesign(this.user, this.pathBar.innerText);
      tags = tags.split(';');
      tagsField.innerText = '';
      for (let i = 0; i < tags.length - 1 ; i++) {
        tagsField.innerText += '-' + tags[i] + '\n';
      }
    } else {
      tagsRow.setAttribute('class', ' hide');
      tagsField.innerText = '';
    }
    this.selectedFile = file;
  }

  refresh() {
    window.location.reload();
  }
}
