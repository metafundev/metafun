import * as CryptoJS from 'crypto-js';
import {WordArray} from 'crypto-js';
import {HttpClient} from '@angular/common/http';
import {FormGroup} from '@angular/forms';
import {Observable} from 'rxjs';
import Axios from 'axios';
export default class ApiUtils {
  private apiUri: string;
  private apiModule: string;
  constructor(apiMod: string) {
    this.apiUri = 'http://localhost:8080/MetaApi/';
    this.apiModule = apiMod;
  }

  async firstUserRequest() {
    const queryString = '/firstUser';
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };

    let myJson = 'ERROR REQUEST: ApiUtils.fakeRequest()';
    const userAction = async () => {
      const response = await fetch(options.uri);
      myJson = await response.json();
    };

    await userAction();
    return myJson;
  }

  async logInRequest(eMailReq: string, passwordReq: WordArray) {
    const queryString = '/logIn?eMail=' + eMailReq + '&password=' + passwordReq.toString();
    let logInSuccess = false;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let logInResponse = null;
    const userAction = async () => {
      const response = await fetch(options.uri);
      logInResponse = await response.text();
    };
    await userAction();
    if (logInResponse === 'true') {
      logInSuccess = true;
    }
    return logInSuccess;
  }

  async changePasswordRequest(eMail: string, password: string, newPassword: string) {

    const queryString = '?eMail=' + eMail + '&password=' + CryptoJS.MD5(password) + '&newPassword=' + CryptoJS.MD5(newPassword);
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };

    let changePasswordResponse = 'ERROR REQUEST: ApiUtils.changePasswordRequest()';
    const userAction = async () => {
      const response = await fetch(options.uri);
      changePasswordResponse = await response.text();
    };

    await userAction();
    return changePasswordResponse;
  }

  async signUpRequest(nameReq: string, eMailReq: string, passwordReq: CryptoJS.WordArray) {
    const queryString = '/signUp?name=' + nameReq + '&eMail=' + eMailReq + '&password=' + passwordReq.toString();
    let logInSuccess = false;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let logInResponse = null;
    const userAction = async () => {
      const response = await fetch(options.uri);
      logInResponse = await response.text();
    };
    await userAction();
    if (logInResponse === 'true') {
      logInSuccess = true;
    }
    return logInSuccess;
  }

  async getUserName(eMailReq: string) {
    const queryString = '/getUserName?eMail=' + eMailReq;
    let name: string = null;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    const userAction = async () => {
      const response = await fetch(options.uri);
      name = await response.text();
    };
    await userAction();
    return name;
  }

  async getUserBymailRequest(eMailReq: string ) {
    const queryString = '/getUser?eMail=' + eMailReq;
    let logInSuccess = false;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let logInResponse = null;
    const userAction = async () => {
      const response = await fetch(options.uri);
      logInResponse = await response.text();
    };
    await userAction();
    if (logInResponse === 'true') {
      logInSuccess = true;
    }
    return logInSuccess;
  }

  /****************************************** **************************************************************************************/
  /****************************************API FILES FUNCTIONS *********************************************************************/
  /********************************************** **********************************************************************************/

  uploadFile(file: File, user: string, nameFile: string, typeFile: string, tech: string, path: string, parent: string): number {
    const reader = new FileReader();
    let status: Promise<number>;
    let base64 = '';

    reader.readAsText(file);
    reader.onload = () => {
      base64 = btoa(reader.result.toString()); // ADDED TOSTRING FOR THINGS
      status = (async () => {
        const rawResponse = await fetch('http://localhost:8080/MetaApi/files/upload', {
          method: 'POST',
          headers: {
            Accept: 'text/plain',
            'Content-Type': 'text/plain'
          },
          body: '{"path":"' + path +
            '","fileName":"' + nameFile +
            '","user":"' + user +
            '","parentDir":"' + parent +
            '","type": "FILE' +
            '","bioFormat": "' + typeFile +
            '","tech":"' + tech +
            '","base64":"' + base64 +
            '"}'
        });
        return rawResponse.status;
      })();
    };
    reader.onerror = error => {
    };
    return 200;
  }


  nguploadFile(fileSend: File, user: string, nameFile: string, typeFile: string, tech: string, path: string, parent: string) {
    console.log (fileSend);
    const data = new FormData();
    data.append('file', fileSend);
    data.append('user', user);
    data.append('nameFile', nameFile);
    data.append('type', typeFile);
    data.append('tech', tech);
    data.append('path', path);
    data.append('parent', parent);
    console.log (path);
    console.log (data.get('path'));
    fetch('http://localhost:8080/MetaApi/files/uploadWithPost', { // Your POST endpoint
      method: 'POST',
      headers: {
        // Content-Type may need to be completely **omitted**
        // or you may need something
        // 'Content-Type': 'multipart/
      },
      body: data // This is your file object
    }).then(
      response => response // if the response is a JSON object
    ).then(
      success => console.log(success) // Handle the success response object
    ).catch(
      error => console.log(error) // Handle the error response object
    );
  }

  async axiosUpload(fileSend: File, user: string, nameFile: string, typeFile: string, tech: string, path: string, parent: string) {
    console.log (fileSend);
    const data = new FormData();
    data.append('file', fileSend);
    data.append('user', user);
    data.append('nameFile', nameFile);
    data.append('type', typeFile);
    data.append('tech', tech);
    data.append('path', path);
    data.append('parent', parent);
    console.log (data.get('path'));
    Axios.post('http://localhost:8080/MetaApi/files/uploadWithPost', data, {
      onUploadProgress: (progressEvent) => {
        if (progressEvent.lengthComputable) {
          console.log(progressEvent.loaded + ' ' + progressEvent.total);
         // this.updateProgressBarValue(progressEvent);
        }
      }
    }).then(
      response => response // if the response is a JSON object
    ).then(
      success => console.log(success) // Handle the success response object
    ).catch(
      error => console.log(error) // Handle the error response object
    );
  }

  async getFilesFromUser(user: string) {
    const queryString = '/filesFromUser?user=' + user;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };

    let myJson = 'ERROR REQUEST: ApiUtils.getFilesFromUser()';
    const userAction = async () => {
      const response = await fetch(options.uri);
      myJson = await response.text();
    };
    await userAction();
    return myJson;
  }

  async getUserFolders(user: string) {
    const queryString = '/getUserFolders?user=' + user;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };

    let myJson = 'ERROR REQUEST: ApiUtils.getUserFolders()';
    const userAction = async () => {
      const response = await fetch(options.uri);
      myJson = await response.text();
    };
    await userAction();
    return myJson;
  }

  async getJobsFromUser(user: string) {
    const queryString = '/jobsFromUser?user=' + user;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let myJson = 'ERROR REQUEST: ApiUtils.jobsFromUser()';
    const userAction = async () => {
      const response = await fetch(options.uri);
      myJson = await response.text();
    };
    await userAction();
    return myJson;
  }

  async getIDfromFile(path: string) {
    const queryString = '/getFileID?path=' + path;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let idFile = '';
    const userAction = async () => {
      const response = await fetch (options.uri);
      idFile = await response.text();
    };
    await userAction();
    return idFile;
  }

  async createFolder(user: string, folderName: string, parentDir: string) {
    const queryString = '/createFolder?folderName=' + folderName + '&user=' + user + '&parentDir=' + parentDir;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let resp = 'false';
    const userAction = async () => {
      const response = await fetch (options.uri);
      resp = await response.text();
    };
    await userAction();
    return resp;
  }


  async getFolderContent(user: string, folderPath: string) {
    const queryString = '/getContentFolder?user=' + user + '&path=' + folderPath;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let content = '';
    const userAction = async () => {
      const response = await fetch (options.uri);
      content = await response.text();
    };
    await userAction();
    return content;
  }

  async getResultContent(user: string, name: string) {
    const queryString = '/getContentResult?user=' + user + '&name=' + name;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let content = '';
    const userAction = async () => {
      const response = await fetch (options.uri);
      content = await response.text();
    };
    await userAction();
    return content;
  }

  async getExperimentalDesign(user: string, filePath: string) {
    const queryString = '/experimentalDesign?user=' + user + '&filePath=' + filePath;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let content = '';
    if (!(user === undefined || user === 'undefined' || filePath === undefined || filePath === 'undefined')) {
      const userAction = async () => {
        const response = await fetch (options.uri);
        content = await response.text();
      };
      await userAction();
    }
    return content;
  }

  async getjobDesign(user: string, jobName: string) {
    const queryString = '/getJobDesign?user=' + user + '&filePath=' + jobName;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let content = '';
    if (!(user === undefined || user === 'undefined' || jobName === undefined || jobName === 'undefined')) {
      const userAction = async () => {
        const response = await fetch (options.uri);
        content = await response.text();
      };
      await userAction();
    }
    return content;
  }

  async getClusering(user: string, filePath: string) {
    const queryString = '/getImg?user=' + user + '&imgPath=' + filePath;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let content = '';
    if (!(user === undefined || user === 'undefined' || filePath === undefined || filePath === 'undefined')) {
      const userAction = async () => {
        const response = await fetch(options.uri);
        content = await response.text();
      };
      await userAction();
    }
    return content;
  }

  async getMetaGoTerm(user: string, jobName: string, GOTerm) {
    const queryString = '/getMetaDataGOterm?user=' + user + '&jobName=' + jobName + '&GOTerm=' + GOTerm;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let content = '';
    if (!(user === undefined || user === 'undefined' || jobName === undefined || jobName === 'undefined' || GOTerm === undefined ||
    GOTerm === 'undefined')) {
      const userAction = async () => {
        const response = await fetch(options.uri);
        content = await response.text();
      };
      await userAction();
    }
    return content;
  }

  async checkExperimentalDesign(user: string, csvFile: string, ddfFile: string) {
    const queryString = '/checkExperimentalDesign?user=' + user + '&csvFile=' + csvFile + '&ddfFile=' + ddfFile;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let content = '';
    const userAction = async () => {
      const response = await fetch (options.uri);
      content = await response.text();
    };
    await userAction();
    return content;
  }

  async deleteFile(user: string, filePath: string) {
    const queryString = '/deleteFile?user=' + user + '&file=' + filePath;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let content = '';
    const userAction = async () => {
      const response = await fetch (options.uri);
      content = await response.text();
    };
    await userAction();
    return content;
  }


  async renameFile(user: string, oldName: string, newName: string) {
    const queryString = '/renameFile?user=' + user + '&oldName=' + oldName + '&newName=' + newName;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let content = '';
    const userAction = async () => {
      const response = await fetch (options.uri);
      content = await response.text();
    };
    await userAction();
    return content;
  }

  async getDiskUsage(user: string) {
    const queryString = '/getDiskUsage?user=' + user;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let content = '';
    const userAction = async () => {
      const response = await fetch (options.uri);
      content = await response.text();
    };
    await userAction();
    return content;
  }



  /*********************************************************************************************************************************/
  /****************************************API JOBS FUNCTIONS *********************************************************************/

  async getJobStatus(jobName: string) {
    const queryString = '/newJob?name=' + jobName;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let status = '';
    const userAction = async () => {
      const response = await fetch (options.uri);
      status = await response.text();
    };
    await userAction();
    return status;
  }

  async deleteJob(user: string, jobName: string) {
    const queryString = '/deleteJob?user=' + user + '&jobName=' + jobName;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let status = '';
    const userAction = async () => {
      const response = await fetch (options.uri);
      status = await response.text();
    };
    await userAction();
    return status;
  }

  async getJobDesign(user: string, jobName: string) {
    const queryString = '/getJobDesign?user=' + user + '&jobName=' + jobName;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let status = '';
    if (!(user === undefined || user === 'undefined' || jobName === undefined || jobName === 'undefined')) {
      const userAction = async () => {
        const response = await fetch(options.uri);
        status = await response.text();
      };
      await userAction();
    }
    return status;
  }

  registerJob(user: string, name: string, exprMatFiles: string[], expDesignFiles: string[], designValues: string,  ops: string) {
    let exprMatFilesNames = '';
    let expDesignFilesNames = '';
    for (let i = 0 ; i < exprMatFiles.length; i++ ) {
      // @ts-ignore
      exprMatFilesNames += exprMatFiles[i] + '@';
      // @ts-ignore
      expDesignFilesNames += expDesignFiles[i].path + '@';
    }
    const res = fetch('http://localhost:8080/MetaApi/jobs/newJob', {
      method: 'POST',
      headers: {
        Accept: 'text/plain',
        'Content-Type': 'text/plain'
      },
      body: 'user:' + user + ', name:' + name + ', exprMatFiles:' + exprMatFilesNames + ', expDesignFilesNames:' + expDesignFilesNames +
        ', designValues:' + designValues + ', ops:' + ops
    });
    return res;
  }

  async downloadJobResult(user: string, filePath: string) {
    const queryString = '/downloadFile?user=' + user + '&filePath=' + filePath;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let status = '';
    const userAction = async () => {
      const response = await fetch (options.uri);
      status = await response.text();
    };
    await userAction();
    return status;
  }

  async getPlotCount(user: string, jobName: string) {
    const queryString = '/getPlotCount?user=' + user + '&jobName=' + jobName;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let status: any = null;
    if (!(user === undefined || user === 'undefined' || jobName === undefined || jobName === 'undefined')) {
      const userAction = async () => {
        const response = await fetch(options.uri);
        status = await response.text();
      };
      await userAction();
    }
    return status;
  }

  async getBoxplotData(user: string, jobName: string) {
    const queryString = '/getBoxplotData?user=' + user + '&jobName=' + jobName;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let status = '';
    if (!(user === undefined || user === 'undefined' || jobName === undefined || jobName === 'undefined')) {
      const userAction = async () => {
        const response = await fetch(options.uri);
        status = await response.text();
      };
      await userAction();
    }
    return status;
  }

  async getPcaplotData(user: string, jobName: string) {
    const queryString = '/getPcaplotData?user=' + user + '&jobName=' + jobName;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let status = '';
    if (!(user === undefined || user === 'undefined' || jobName === undefined || jobName === 'undefined')) {
      const userAction = async () => {
        const response = await fetch(options.uri);
        status = await response.text();
      };
      await userAction();
    }
    return status;
  }

  async getTopDiffExpData(user: string, jobName: string) {
    const queryString = '/getDiffExpTopData?user=' + user + '&jobName=' + jobName;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let status = '';
    if (!(user === undefined || user === 'undefined' || jobName === undefined || jobName === 'undefined')) {
      const userAction = async () => {
        const response = await fetch(options.uri);
        status = await response.text();
      };
      await userAction();
    }
    return status;
  }

  async getGSEAData(user: string, jobName: string) {
    const queryString = '/getGSEAData?user=' + user + '&jobName=' + jobName;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let status = '';
    if (!(user === undefined || user === 'undefined' || jobName === undefined || jobName === 'undefined')) {
      const userAction = async () => {
        const response = await fetch(options.uri);
        status = await response.text();
      };
      await userAction();
    }
    return status;
  }

  async getHipathiaData(user: string, jobName: string) {
    const queryString = '/getHipathiaData?user=' + user + '&jobName=' + jobName;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let status = '';
    if (!(user === undefined || user === 'undefined' || jobName === undefined || jobName === 'undefined')) {
      const userAction = async () => {
        const response = await fetch(options.uri);
        status = await response.text();
      };
      await userAction();
    }
    return status;
  }

  async getMetaTableData(user: string, jobName: string) {
    const queryString = '/getMetaData?user=' + user + '&jobName=' + jobName;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let status = '';
    if (!(user === undefined || user === 'undefined' || jobName === undefined || jobName === 'undefined')) {
      const userAction = async () => {
        const response = await fetch(options.uri);
        status = await response.text();
      };
      await userAction();
    }
    return status;
  }

  async getStudiesNames(user: string, jobName: string) {
    const queryString = '/getStudiesNames?user=' + user + '&jobName=' + jobName;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let status = '';
    if (!(user === undefined || user === 'undefined' || jobName === undefined || jobName === 'undefined')) {
      const userAction = async () => {
        const response = await fetch(options.uri);
        status = await response.text();
      };
      await userAction();
    }
    return status;
  }

  async getSummaries(user: string, jobName: string) {
    const queryString = '/getSummaries?user=' + user + '&jobName=' + jobName;
    const options = {
      uri: this.apiUri + this.apiModule + queryString,
    };
    let status = '';
    if (!(user === undefined || user === 'undefined' || jobName === undefined || jobName === 'undefined')) {
      const userAction = async () => {
        const response = await fetch(options.uri);
        status = await response.text();
      };
      await userAction();
    }
    return status;
  }
}
