import {Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges} from '@angular/core';
import ApiUtils from '../../utils/ApiUtils';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit, OnChanges {
  stdNames;
  user;
  designHeader;
  summaryDesign;
  studyNames = [];
  @Input() selectedAnalysis;
  private _selectedAnalysis;
  stdCount = 0;
  diffExpHeader: any;
  totalSamples = [];
  contrast: any;
  summaryDiffExp: any;
  funcProfileHeader: any;
  summaryFuncProfile: any;
  metaHeader: any;
  summaryMeta: any;
  public summaryBarplot = {
    data: [],
    layout: {
      title: 'Sample Description',
      showlegend: true,
      barmode: 'stack'
    }
  };
  constructor(private cookieService: CookieService) { }

  ngOnInit() {
    this.user = this.cookieService.get('userLoged');
  }

  async ngOnChanges(changes: SimpleChanges) {
    const selectedAnalysis: SimpleChange = changes.selectedAnalysis;
    this._selectedAnalysis = selectedAnalysis.currentValue;
    this.summaryBarplot = {
      data: [],
      layout: {
        title: 'Sample Description',
        showlegend: true,
        barmode: 'stack'
      }
    };
    if (this._selectedAnalysis !== undefined && !this._selectedAnalysis.match('undefined')) {
      const stdNamess = [];
      const api = new ApiUtils('files');
      const summariesResponse = await api.getSummaries(this.user, this.selectedAnalysis);
      this.contrast = await api.getJobDesign(this.user, this.selectedAnalysis);
      const re = /_/gi;
      const splitedSummaries = summariesResponse.split('#');

      const rawDesign = splitedSummaries[0];
      const rawDifExp = splitedSummaries[1];
      const rawFunPro = splitedSummaries[2];
      const rawMeta = splitedSummaries[3] + splitedSummaries[4].split('\n')[2] + '\n' + splitedSummaries[5].split('\n')[2] + '\n';
      console.log (rawMeta);
      this.designHeader = rawDesign.split('\n')[0].split(',');
      this.designHeader[0] = 'Study Name';
      for (let i = 0 ; i < this.designHeader.length; i++) {
        this.designHeader[i] = this.designHeader[i].replace(re, ' ');
        this.totalSamples.push(0);
      }
      const auxsummaryDesign = rawDesign.split('\n');
      auxsummaryDesign.pop();
      auxsummaryDesign.shift();
      this.summaryDesign = auxsummaryDesign;
      const traces = [];
      for (let i = 0 ; i < auxsummaryDesign.length; i++) {
        stdNamess.push(auxsummaryDesign[i].split(',')[0]);
        for (let h = 1; h < this.designHeader.length; h++) {
          this.totalSamples[h] += parseInt(auxsummaryDesign[i].split(',')[h]);
        }
      }
      const colors = ['#808080', '#FFA500', '#009E73', '#56B4E9'];
      for (let i = 0 ; i < this.summaryDesign.length; i++) {
        let tempTrace = {};
        const samplesNum = this.summaryDesign[i].split(',');
        tempTrace = {
          x: stdNamess,
          y: samplesNum.slice(1, samplesNum.length - 1),
          name: this.designHeader[i + 1],
          type: 'bar',
          marker: {
            color: colors[i]
          },
        };
        this.summaryBarplot.data.push(tempTrace);
      }
      console.log (this.totalSamples);
      console.log (this.summaryBarplot);


      this.diffExpHeader = rawDifExp.split('\n')[1].split(',');
      this.diffExpHeader[0] = 'Study Name';
      const diffexpAux = rawDifExp.split('\n');
      diffexpAux.shift();
      diffexpAux.shift();
      diffexpAux.pop();
      this.summaryDiffExp = diffexpAux;

      this.funcProfileHeader = rawFunPro.split('\n')[1].split(',');
      this.funcProfileHeader[0] = 'Study Name';
      const funcProfAux = rawFunPro.split('\n');
      funcProfAux.shift();
      funcProfAux.shift();
      funcProfAux.pop();
      this.summaryFuncProfile = funcProfAux;

      this.metaHeader = rawMeta.split('\n')[1].split(',');
      this.metaHeader[0] = 'Ontology';
      const metaAus = rawMeta.split('\n');
      metaAus.shift();
      metaAus.shift();
      metaAus.pop();
      this.summaryMeta = metaAus;
      }
  }

}
