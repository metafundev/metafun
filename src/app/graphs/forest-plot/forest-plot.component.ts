import {Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges} from '@angular/core';
import ApiUtils from '../../utils/ApiUtils';
import {CookieService} from 'ngx-cookie-service';


@Component({
  selector: 'app-forest-plot',
  templateUrl: './forest-plot.component.html',
  styleUrls: ['./forest-plot.component.css']
})
export class ForestPlotComponent implements OnInit {
  @Input() user;
  public debug = false;
  public useResizeHandler = true;
  private stds;
  @Input() summaryLor: string;
  @Input() summaryci: string;
  @Input() id: string;
  @Input() selectedAnalysis: string;
  @Input() selectedGO: string;
  private layout = {
    showlegend: false,

    xaxis: {
      showline: false,
      zeroline: true,
      showgrid: false,
      showticklabels: false,
      linecolor: 'black',
      linewidth: 1,
      autotick: true,
      rangemode: 'tozero',
      autorange: false,
      zerolinewidth: 2,
      range: [],
      ticks: '',
      tickcolor: 'rgb(204,204,204)',
      tickwidth: 0,
      ticklen: 1,
      tickfont: {
        family: 'Arial',
        size: 12,
        color: 'rgb(82, 82, 82)'
      }
    },
    yaxis: {
      showgrid: false,
      zeroline: true,
      zerolinewidth: 1,
      showline: false,
      showticklabels: false,
      text: '',
      range: [-15, 30]
    },
    autosize: true,

    margin: {
      autoexpand: false,
      l: 100,
      r: 20,
      t: 100
    },

    annotations: [ ]
  };

  public forestPlot = {

    data: [

    ],
    layout: this.layout,
    config: {
    },
  };

  constructor(private cookieService: CookieService) {}

  async ngOnInit() {
    const api = new ApiUtils('files');
    this.forestPlot.config = {
      staticPlot: false,
      responsive: true,
      autosizable: true,
      doubleClick: false,
      toImageButtonOptions: {
        filename: this.selectedGO + 'forest'
      },
      showAxisDragHandles: false,
      showAxisRangeEntryBoxes: false,
    };
    const annot = {
      xref: 'paper',
      yref: 'paper',
      x: 0.0,
      y: 1.05,
      xanchor: 'left',
      yanchor: 'bottom',
      text: this.selectedGO,
      font: {
        family: 'Arial',
        size: 26,
        color: 'rgb(37,37,37)'
      },
      showarrow: false
    };
    this.layout.annotations.push(annot);
    let forestData = await api.getMetaGoTerm(this.user, this.selectedAnalysis, this.selectedGO);
    forestData = JSON.parse(forestData);
    this.stds = await api.getStudiesNames(this.user, this.selectedAnalysis);
    this.drawForest(forestData);
  }

  async drawForest(forestData) {
    const api = new ApiUtils('files');
    const goForestData = JSON.parse(forestData);
    const stdsAux = this.stds.split('\n');
    const stdsArr = [];
    let lowerRange = 99;
    let upperRange = -99;
    for (let i = 0 ; i < stdsAux.length - 1; i++) {
      stdsArr.push(stdsAux[i].split('\t')[1]);
    }
    for (let i = 0; i < stdsArr.length; i++) {
      const stdGO = stdsArr[i];
      const pesos = [];
      if (goForestData[stdGO] !== undefined && goForestData[stdGO] !== 'undefined') {
        for (let k = 0 ; k < stdsArr.length; k++) {
          if (goForestData[stdsArr[k]] !== undefined && goForestData[stdsArr[k]] !== 'undefined') {
            pesos.push(+goForestData[stdsArr[k]].weight);
          }
        }
        const sizes = this.normalizeWeights(pesos);
        const trace = {
          x: [goForestData[stdGO].cilb, goForestData[stdGO].lor, goForestData[stdGO].ciub],
          y: [(i * 5) + 10, (i * 5) + 10, (i * 5) + 10],
          mode: 'lines+markers',
          marker: {
            size: [5, sizes[i], 5],
            symbol: ['square', 'square', 'square'],
            color: 'black'
          },
          textposition: 'top',
          line: {
            color: 'black',
            width: 2
          },
          hoverinfo: 'x',
          type: 'scatter',
          name: stdsArr[i]
        };
        const annotTrace = {
          xref: 'paper',
          x: 0.05,
          y: trace.y[0],
          xanchor: 'right',
          yanchor: 'middle',
          text: stdsArr[i],
          showarrow: false,
          font: {
            family: 'Arial',
            size: 16,
            color: 'black'
          }
        };
        const annotTrace1 = {
          xref: 'paper',
          x: 1,
          y: trace.y[0],
          xanchor: 'right',
          yanchor: 'middle',
          text: goForestData[stdGO].lor.toFixed(2) + ' [' + goForestData[stdGO].cilb.toFixed(2) +
            ', ' + goForestData[stdGO].ciub.toFixed(2) + ']',
          showarrow: false,
          font: {
            family: 'Arial',
            size: 12,
            color: 'black'
          }
        };
        goForestData[stdGO].cilb < lowerRange ? lowerRange = goForestData[stdGO].cilb : lowerRange = lowerRange;
        goForestData[stdGO].ciub > upperRange ? upperRange = goForestData[stdGO].ciub : upperRange = upperRange;
        this.forestPlot.data.push(trace);
        this.forestPlot.layout.annotations.push(annotTrace, annotTrace1);
      }
    }
    const model = {
      lor: +this.summaryLor,
      cilb: +this.summaryci.split(',')[0].trim().replace('[', ''),
      ciub:	+this.summaryci.split(',')[1].trim().replace(']', '')
    };
    this.layout.xaxis.range = [lowerRange - 0.5, upperRange + 0.5];
    this.layout.yaxis.range = [-15, this.stds.length - 1 * 5];
    this.appendSummary(model);
  }

  appendSummary(summaryModel) {
    const trace = {
      x: [summaryModel.cilb, summaryModel.lor, summaryModel.ciub],
      y: [-5, -5, -5],
      mode: 'lines+markers',
      marker: {
        size: [5, 10, 5],
        symbol: ['', 'diamond', ''],
        color: ['red', 'red', 'red']
      },
      textposition: 'top',
      line: {
        color: 'red',
        width: 2
      },
      hoverinfo: 'x',
      type: 'scatter'
    };
    const annotTrace = {
      xref: 'paper',
      x: 0.05,
      y: trace.y[0],
      xanchor: 'right',
      yanchor: 'middle',
      text: 'Summary',
      showarrow: false,
      font: {
        family: 'Arial',
        size: 16,
        color: 'black'
      }
    };
    const annotTrace1 = {
      xref: 'paper',
      x: 1,
      y: trace.y[0],
      xanchor: 'right',
      yanchor: 'middle',
      text: summaryModel.lor.toFixed(2) + ' [' + summaryModel.cilb.toFixed(2) +
        ', ' + summaryModel.ciub.toFixed(2) + ']',
      showarrow: false,
      font: {
        family: 'Arial',
        size: 12,
        color: 'black'
      }
    };
    this.forestPlot.data.push(trace);
    this.forestPlot.layout.annotations.push(annotTrace, annotTrace1);
  }

  normalizeWeights(weights) {
    const normWeights = [];
    let maxim = -999;
    let minim = 999;
    const l = weights.length;
    if (l > 2) {
      for (let i = 0; i < l ; i++) {
        weights[i] > maxim ? maxim = weights[i] : maxim = maxim;
        weights[i] < minim ? minim = weights[i] : minim = minim;
      }
      const ratio = maxim / minim;
      let val = 0;
      for (let i = 0; i < l; i++) {
        val = 7 + (weights[i] - minim) * (15 - 7) / (maxim - minim);
        normWeights.push(val);
      }
    } else {
      if (weights[0] > weights[1]) {
        normWeights.push(15, 7);
      } else {
        normWeights.push(7, 15);
      }
    }

    return normWeights;
  }

}

