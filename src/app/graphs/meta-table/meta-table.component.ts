import {Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';
import ApiUtils from '../../utils/ApiUtils';
import {saveAs} from '../../../../node_modules/file-saver';
@Component({
  selector: 'app-meta-table',
  templateUrl: './meta-table.component.html',
  styleUrls: ['./meta-table.component.css']
})
export class MetaTableComponent implements OnInit, OnChanges {
  @Input() selectedAnalysis: string;
  private user;
  private goData;
  // tslint:disable-next-line:variable-name
  public _selectedAnalysis: string;
  private parsedData = null;
  constructor(private cookieService: CookieService, private router: Router) { }

  async ngOnInit() {
    this.user = this.cookieService.get('userLoged');
    document.getElementById('downloadMetaButton').addEventListener('click', (e: Event) => this.downloadMeta());
    const api = new ApiUtils('files');
    const data = await api.getMetaTableData(this.user, this._selectedAnalysis);
    if (data !== '') {
      this.parsedData = JSON.parse(data);
      this.goData = this.parsedData.data;
    }

  }

  async ngOnChanges(changes: SimpleChanges) {
    const selectedAnalysis: SimpleChange = changes.selectedAnalysis;
    this._selectedAnalysis = selectedAnalysis.currentValue;
    const api = new ApiUtils('files');
    const data = await api.getMetaTableData(this.user, this._selectedAnalysis);
    if (data !== '') {
      this.parsedData = JSON.parse(data);
      this.goData = this.parsedData.data;
    }
  }

  async downloadMeta() {
    const api = new ApiUtils('files');
    const file = await api.downloadJobResult(this.user, this.selectedAnalysis + '/metaRes/all.results.DL.txt');
    const downFile = new File([file], this.selectedAnalysis + 'MetaAnalysis.csv',
      {type: 'text/plain;charset=utf-8'});
    saveAs(downFile);
  }
}
