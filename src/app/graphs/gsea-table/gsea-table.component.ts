import {Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {saveAs} from '../../../../node_modules/file-saver';
import {Router} from '@angular/router';
import ApiUtils from '../../utils/ApiUtils';

@Component({
  selector: 'app-gsea-table',
  templateUrl: './gsea-table.component.html',
  styleUrls: ['./gsea-table.component.css']
})
export class GseaTableComponent implements OnInit, OnChanges {
  constructor(private cookieService: CookieService, private router: Router) { }
  private static orderedbyLor = 1;
  private static orderedbypVal = 1;
  private static orderedbyadj = 1;

  @Input() selectedAnalysis: string;
  @Input() gseaFlag: boolean;
  private user;
  private tableData;
  private studyIndex = 0;
  private init = 1;
  // tslint:disable-next-line:variable-name
  private _selectedAnalysis: string;
  private stdNames = null;
  selectedOntology = 'bp';
  searchVal: any;
  searchPVal: any;

  ngOnInit() {
    if (this.gseaFlag) {
      this.user = this.cookieService.get('userLoged');
      const downloadButton = document.getElementById('downloadGSEAButton')
        .addEventListener('click', (e: Event) => this.downloadGSEA());
    }
    // this.getPcaPlotValues();
  }

  async ngOnChanges(changes: SimpleChanges) {
    if (this.gseaFlag) {
      const api = new ApiUtils('files');
      const selectedAnalysis: SimpleChange = changes.selectedAnalysis;
      this.stdNames = null;
      this.cleanData();
      const selectStydyInput = document.getElementById('gseaStudySelect');
      selectStydyInput.addEventListener('change', (e: Event) => this.studySelection());
      this._selectedAnalysis = selectedAnalysis.currentValue;
      if (!(this._selectedAnalysis === undefined) && !(this._selectedAnalysis === 'undefined')) {
        const aux = await api.getStudiesNames(this.user, this._selectedAnalysis);
        this.stdNames = aux.split('\n');
        selectStydyInput.innerHTML = '<option value="" disabled selected>Choose your study</option>';
        for (let i = 0; i < this.stdNames.length - 1; i++) {
          selectStydyInput.innerHTML += '<option value="' + i + '">' + this.stdNames[i].split('\t')[1] + '</option>';
        }
        this.selectOntology(this.selectedOntology, this.studyIndex);
      }
    }
  }

  studySelection() {
    const studyselect = document.getElementById('gseaStudySelect') as HTMLOptionElement;
    this.cleanData();
    this.studyIndex = +studyselect.value;
    console.log (this.studyIndex);
    this.selectOntology(this.selectedOntology, this.studyIndex);
  }

  async getGSEADataTables(index: number) {
    const api = new ApiUtils('files');
    const data = await api.getGSEAData(this.user, this._selectedAnalysis + '/topGsea' + (index + 1) + '.txt');
    const parsedData = JSON.parse(data);
    this.tableData = parsedData.data;
    this.createTable(parsedData.data, index);
  }

  createTable(data, index) {
    this.cleanData();
    let newTable = '<div class="s10 pull-s1 push-s1">' +
      '<table name="myTable" class="responsive-table highlight"><thead>\n' +
      '    <tr>\n' +
      '      <th class="center">GO ID</th>\n' +
      '      <th class="">GO Term</th>\n' +
      '      <th id="gseaLorCol" class="center metaPointer">LOR</th>\n' +
      '      <th id="gseaPvalCol" class="center metaPointer">P value</th>\n' +
      '      <th id="gseaAdjPvalCol" class="center metaPointer">Adjusted P value</th>\n' +
      '      <th id="gseaNcol" class="center metaPointer">N</th>\n' +
      '    </tr>\n' +
      '    </thead>\n' +
      '    <tbody id="jobsTable' + index + '">';
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < data.length; i++) {
      newTable += '<tr id="gseaRow' + i + '">\n' +
        '      <td class="center"><a href = https://www.ebi.ac.uk/QuickGO/term/' + data[i].GOTerm + ' target="_blank"</a>' +
        data[i].GOTerm + '</td>\n' +
        '      <td class="">' + data[i].GOName + '</td>\n' +
        '      <td class="center">' + data[i].lor.toFixed(3) + '</td>\n' +
        '      <td class="center">' + data[i].pval.toExponential(4) + '</td>\n' +
        '      <td class="center">' + data[i].adjPval.toExponential(4) + '</td>\n' +
        '      <td class="center">' + data[i].N + '</td>\n' +
        '    </tr>';
    }
    newTable += '</tbody></table></div>';
    document.getElementById('gseaTables').innerHTML += newTable;
    document.getElementById('gseaLorCol').addEventListener('click', (e: Event) => this.orderByLORUP());
    document.getElementById('gseaPvalCol').addEventListener('click', (e: Event) => this.orderByPvalueUP());
    document.getElementById('gseaAdjPvalCol').addEventListener('click', (e: Event) => this.orderByadjPvalueUP());
  }

  cleanData() {
    const gseaTables = document.getElementById('gseaTables');
    gseaTables.innerHTML = '';
  }

  async downloadGSEA() {
    const api = new ApiUtils('files');
    const file = await api.downloadJobResult(this.user, this.selectedAnalysis + '/gsea' + (this.studyIndex + 1) + '.txt');
    const downFile = new File([file], this.stdNames[this.studyIndex] + 'gsea.csv', {type: 'text/plain;charset=utf-8'});
    saveAs(downFile);
  }

  checkSearchVal() {
    const a = document.getElementById('search-inputGSA') as HTMLInputElement;
    let index = 0;
    for (const value of this.tableData) {
      if ((value.GOTerm.includes(a.value) && a.value !== '') || (value.GOName.includes(a.value) && a.value !== '')) {
        document.getElementById('gseaRow' + index).setAttribute('style', 'background-color: wheat;');
      } else {
        document.getElementById('gseaRow' + index).setAttribute('style', 'background-color: white');
      }
      index++;
    }
  }

  orderByLORUP() {
    if (GseaTableComponent.orderedbyLor === -1) {
      this.orderByLORDOWN();
    } else {
      GseaTableComponent.orderedbyLor = -1;
      this.tableData.sort((a , b) => {
        if (a.lor > b.lor) {
          return 1;
        }
        if (b.lor > a.lor) {
          return -1;
        }
        return 0;
      });
    }
    this.createTable(this.tableData, this.studyIndex);
  }
  orderByLORDOWN() {
    GseaTableComponent.orderedbyLor = 1;
    this.tableData.sort((a , b) => {
      if (a.lor > b.lor) {
        return -1;
      }
      if (b.lor > a.lor) {
        return 1;
      }
      return 0;
    });
    this.createTable(this.tableData, this.studyIndex);
  }

  orderByPvalueUP() {
    console.log('orderByPval');
    if (GseaTableComponent.orderedbypVal === -1) {
      this.orderByPvalueDOWN();
    } else {
      GseaTableComponent.orderedbypVal = -1;
      this.tableData.sort((a, b) => {
        if (a.pval > b.pval) {
          return 1;
        }
        if (b.pval > a.pval) {
          return -1;
        }
        return 0;
      });
    }
    this.createTable(this.tableData, this.studyIndex);
  }

  orderByPvalueDOWN() {
    console.log('orderByPval');
    GseaTableComponent.orderedbypVal = 1;
    this.tableData.sort((a , b) => {
      if (a.pval > b.pval) {
        return -1;
      }
      if (b.pval > a.pval) {
        return 1;
      }
      return 0;
    });
    this.createTable(this.tableData, this.studyIndex);
  }

  orderByadjPvalueUP() {
    console.log('orderByPAdj');
    if (GseaTableComponent.orderedbyadj === -1) {
      this.orderByadjPvalueDOWN();
    } else {
      GseaTableComponent.orderedbyadj = -1;
      this.tableData.sort((a, b) => {
        if (a.adjPval > b.adjPval) {
          return 1;
        }
        if (b.adjPval > a.adjPval) {
          return -1;
        }
        return 0;
      });
    }
    this.createTable(this.tableData, this.studyIndex);
  }

  orderByadjPvalueDOWN() {
    console.log('orderByPAdj');
    GseaTableComponent.orderedbyadj = 1;
    this.tableData.sort((a , b) => {
      if (a.adjPval > b.adjPval) {
        return -1;
      }
      if (b.adjPval > a.adjPval) {
        return 1;
      }
      return 0;
    });
    this.createTable(this.tableData, this.studyIndex);
  }

  checkSearchPVal() {
    const pvalsearch = document.getElementById('filter-inputPGSA') as HTMLInputElement;
    let index = 0;
    for (const value of this.tableData) {
      if ((parseFloat(value.adjPval) < parseFloat(pvalsearch.value)) && pvalsearch.value !== '') {
        document.getElementById('gseaRow' + index).setAttribute('style', 'background-color: wheat;');
      } else {
        document.getElementById('gseaRow' + index).setAttribute('style', 'background-color: white');
      }
      index++;
    }
  }

  async selectOntology(ontology: string, index: number) {
    console.log('asdf');
    if (isNaN(index)) {
      index = 0;
    }
    console.log (ontology, this.studyIndex);
    this.selectedOntology = ontology;
    const ontologies = ['bp', 'mf', 'cc'];
    const api = new ApiUtils('files');
    const data = await api.getGSEAData(this.user, this._selectedAnalysis + '/top' + ontology + 'Gsea' + (this.studyIndex + 1) + '.txt');
    const parsedData = JSON.parse(data);
    for (let i = 0 ; i < ontologies.length ; i++) {
      document.getElementById(ontologies[i] + 'Tab').setAttribute('class', '');
    }
    document.getElementById(ontology + 'Tab').setAttribute('class', 'active');
    this.tableData = parsedData.data;
    this.createTable(parsedData.data, this.studyIndex);
  }
}
