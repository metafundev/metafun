import {Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges} from '@angular/core';
import ApiUtils from '../../utils/ApiUtils';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-clusterplot',
  templateUrl: './clusterplot.component.html',
  styleUrls: ['./clusterplot.component.css']
})
export class ClusterplotComponent implements OnInit, OnChanges {
  @Input() selectedAnalysis: string;
  @Input() stdID: string;
  private user;
  private stdNames = null;
  imgSrc: string;
  constructor(private cookieService: CookieService) { }

  async ngOnInit() {
    this.user = this.cookieService.get('userLoged');
  }

  async ngOnChanges() {
    const api = new ApiUtils('files');
    const aux = await api.getStudiesNames(this.user, this.selectedAnalysis);
    this.stdNames = aux.split('\n');

    if (!(this.selectedAnalysis === undefined) || !(this.selectedAnalysis === 'undefined')) {
      this.imgSrc = 'http://localhost:8080/MetaApi/files/getImage?user=' + this.user + '&imgPath=' + this.selectedAnalysis + '/clustering' +
        (this.stdID + 1) + '.png';
    }
  }
}
