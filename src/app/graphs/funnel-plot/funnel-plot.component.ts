import {Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import ApiUtils from '../../utils/ApiUtils';
import {summaryFileName} from '@angular/compiler/src/aot/util';

@Component({
  selector: 'app-funnel-plot',
  templateUrl: './funnel-plot.component.html',
  styleUrls: ['./funnel-plot.component.css']
})
export class FunnelPlotComponent implements OnInit {
  @Input() user;
  public debug = false;
  public useResizeHandler = true;
  private stds;
  @Input() summaryLor: string;
  @Input() summarySE: string;
  @Input() id: string;
  @Input() selectedAnalysis: string;
  @Input() selectedGO: string;
  private layout = {
    showlegend: false,
    title: this.selectedGO,
    xaxis: {
      showline: true,
      zeroline: false,
      showgrid: true,
      showticklabels: true,
      linecolor: 'black',
      linewidth: 1,
      autotick: true,
      autorange: false,
      zerolinewidth: 2,
      range: [],
      title: 'Log Odd Ratio (LOR)',
      ticks: '',
      tickcolor: 'rgb(204,204,204)',
      tickwidth: 0,
      ticklen: 1,
      tickfont: {
        family: 'Arial',
        size: 12,
        color: 'rgb(82, 82, 82)'
      }
    },
    yaxis: {
      showgrid: true,
      zeroline: false,
      zerolinewidth: 1,
      showline: true,
      autorange: false,
      showticklabels: true,
      title: 'Standard Error (SE)',
      range: []
    },
    autosize: true,
    margin: {
      autoexpand: false,
      l: 100,
      r: 20,
      t: 100
    },

    annotations: [ ]
  };
  public funnelPlot = {

    data: [

    ],
    layout: this.layout,
    config: {}
  };
  constructor(private cookieService: CookieService) { }

  async ngOnInit() {
    const api = new ApiUtils('files');
    this.funnelPlot.config =  {
      staticPlot: false,
      responsive: true,
      autosizable: true,
      doubleClick: false,
      toImageButtonOptions: {
        filename: this.selectedGO + 'funnel'
      },
      showAxisDragHandles: false,
      showAxisRangeEntryBoxes: false
    };
    if (this.selectedAnalysis !== undefined && this.selectedAnalysis !== 'undefined') {
      this.stds = await api.getStudiesNames(this.user, this.selectedAnalysis);
      const annot = {
        xref: 'paper',
        yref: 'paper',
        x: 0.0,
        y: 1.05,
        xanchor: 'left',
        yanchor: 'bottom',
        text: this.selectedGO,
        font: {
          family: 'Arial',
          size: 26,
          color: 'rgb(37,37,37)'
        },
        showarrow: false
      };
      this.layout.annotations.push(annot);
      let funnelData = await api.getMetaGoTerm(this.user, this.selectedAnalysis, this.selectedGO);
      funnelData = JSON.parse(funnelData);
      this.drawFunnel(funnelData);
    }
  }

  private drawFunnel(funnelData: any) {
    const goFunneldata = JSON.parse(funnelData);
    const newStdPlot = {x: [], y: [], text: [], font: {color: 'white'}, mode: 'markers', type: 'scatter',
      marker: {size: 10, color: '#56B4E9'}};

    const stdsAux = this.stds.split('\n');
    const stdsArr = [];
    for (let i = 0 ; i < stdsAux.length - 1; i++) {
      stdsArr.push(stdsAux[i].split('\t')[1]);
    }
    for (let i = 0 ; i < stdsArr.length; i++) {
      const stdGO = stdsArr[i];
      if (goFunneldata[stdGO] !== undefined && goFunneldata[stdGO] !== 'undefined') {
        newStdPlot.x.push(goFunneldata[stdGO].lor);
        newStdPlot.y.push(goFunneldata[stdGO].se);
        newStdPlot.text.push(stdGO);
      }
    }
    this.funnelPlot.data.push(newStdPlot);
    const maxSERepresented = (Math.max(...newStdPlot.y) + Math.max(...newStdPlot.y) * 0.05);
    const limSup = 1.96 * maxSERepresented + +this.summaryLor;
    const limInf = -1.96 * maxSERepresented + +this.summaryLor;
    const mid = +this.summaryLor;
    const triangle = {
      x: [limInf, mid, limSup],
      y: [maxSERepresented, 0, maxSERepresented],
      mode: 'lines',
      line: {
        dash: 'dot',
        color: 'black',
        width: 2
      },
      hoverinfo: 'skip'
    };
    const midLine = {
      x: [mid, mid],
      y: [0, maxSERepresented],
      mode: 'lines',
      line: {
        color: 'black',
        width: 1
      },
      hoverinfo: 'skip'
    };
    const dist = (limSup - limInf) * 0.5;
    this.layout.yaxis.range = [maxSERepresented, -0.01];
    this.layout.xaxis.range = [limInf - dist, limSup + dist];
    this.funnelPlot.data.push(triangle);
    this.funnelPlot.data.push(midLine);
  }
}
