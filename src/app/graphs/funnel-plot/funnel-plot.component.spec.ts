import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FunnelPlotComponent } from './funnel-plot.component';

describe('FunnelPlotComponent', () => {
  let component: FunnelPlotComponent;
  let fixture: ComponentFixture<FunnelPlotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FunnelPlotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FunnelPlotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
