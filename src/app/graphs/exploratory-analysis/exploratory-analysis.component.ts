import {Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges} from '@angular/core';
import ApiUtils from '../../utils/ApiUtils';
import {CookieService} from 'ngx-cookie-service';
import {NgxSmartLoaderService} from 'ngx-smart-loader';

@Component({
  selector: 'app-exploratory-analysis',
  templateUrl: './exploratory-analysis.component.html',
  styleUrls: ['./exploratory-analysis.component.css']
})
export class ExploratoryAnalysisComponent implements OnInit, OnChanges {
  stdNames;
  user;
  @Input() selectedAnalysis;
  private _selectedAnalysis;
  stdCount = 0;
  constructor(private cookieService: CookieService, private loader: NgxSmartLoaderService) { }

  async ngOnInit() {
    this.user = this.cookieService.get('userLoged');
    this.loader.start('jobsLoader');
  }

  async ngOnChanges(changes: SimpleChanges) {
    let count = 0;
    const selectedAnalysis: SimpleChange = changes.selectedAnalysis;
    this._selectedAnalysis = selectedAnalysis.currentValue;
    const api = new ApiUtils('files');
    count = await api.getPlotCount(this.user, this._selectedAnalysis);
    const stdsData = await api.getStudiesNames(this.user, this._selectedAnalysis);
    this.stdNames = stdsData.split('\n');
    for(let j = 0 ; j < this.stdNames.length ; j++) {
      this.stdNames[j] = this.stdNames[j].split('\t')[1];
    }
    this.stdNames.pop();
    this.stdCount = +count;
  }
}
