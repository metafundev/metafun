
import {Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges} from '@angular/core';
import ApiUtils from '../../utils/ApiUtils';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-pcaplot',
  templateUrl: './pcaplot.component.html',
  styleUrls: ['./pcaplot.component.css']
})
export class PcaplotComponent implements OnInit, OnChanges {
  @Input() selectedAnalysis: string;
  public debug = true;
  public useResizeHandler = true;
  private user;
  private contrast = '';
  private stdNames = [];
  private pcaValues = [];

  public plot = {
    data: [],
    layout: {
      title: 'PCA Plot',
      showlegend: true
    }
  };
  public plot1 = {
    data: [],
    layout: {
      title: 'PCA Plot',
      showlegend: true
    }
  };
  public plot2 = {
    data: [],
    layout: {
      title: 'PCA Plot',
      showlegend: true
    }
  };
  public plot3 = {
    data: [],
    layout: {
      title: 'PCA Plot',
      showlegend: true
    }
  };
  public plot4 = {
    data: [],
    layout: {
      title: 'PCA Plot',
      showlegend: true
    }
  };
  public plot5 = {
    data: [],
    layout: {
      title: 'PCA Plot',
      showlegend: true
    }
  };
  public plot6 = {
    data: [],
    layout: {
      title: 'PCA Plot',
      showlegend: true
    }
  };
  public plot7 = {
    data: [],
    layout: {
      title: 'PCA Plot',
      showlegend: true
    }
  };
  public plot8 = {
    data: [],
    layout: {
      title: 'PCA Plot',
      showlegend: true
    }
  };
  public plot9 = {
    data: [],
    layout: {

      title: 'PCA Plot',
      showlegend: true
    }
  };
  public plot10 = {
    data: [],
    layout: {
      title: 'PCA Plot',
      showlegend: true
    }
  };
  public plot11 = {
    data: [],
    layout: {
      title: 'PCA Plot',
      showlegend: true
    }
  };
  public plot12 = {
    data: [],
    layout: {
      title: 'PCA Plot',
      showlegend: true
    }
  };
  public plot13 = {
    data: [],
    layout: {
      title: 'PCA Plot',
      showlegend: true
    }
  };
  public plot14 = {
    data: [],
    layout: {
      title: 'PCA Plot',
      showlegend: true
    }
  };

  // tslint:disable-next-line:variable-name
  private _selectedAnalysis = '';
  constructor(private cookieService: CookieService, private router: Router) { }

  ngOnInit() {
    this.user = this.cookieService.get('userLoged');
    // this.getPcaPlotValues();
  }
  async ngOnChanges(changes: SimpleChanges) {
    const apiJ = new ApiUtils('jobs');
    const apiF = new ApiUtils('files');
    const selectedAnalysis: SimpleChange = changes.selectedAnalysis;
    this._selectedAnalysis = selectedAnalysis.currentValue;
    this.contrast = await apiJ.getJobDesign(this.user, this._selectedAnalysis);

    this.cleanData();

    // Get Plot Count for analysis
    let count = 2;
    const sCount = await apiF.getPlotCount(this.user, this._selectedAnalysis);
    count = +sCount;

    for (let i = 0; i < count; i++) {
      await this.getPcaPlotValues(i);
    }
    this.printPlots();

  }
  async getPcaPlotValues(index) {
    const controlTrace = {x: [], y: [], text: [], name: 'Control', mode: 'markers', type: 'scatter',
      marker: {size: 12, color: '#FFA500'}};
    const caseTrace = {x: [], y: [], text: [], name: 'Case', mode: 'markers', type: 'scatter',
      marker: {size: 12, color: '#808080'}};
    const METrace = {x: [], y: [], text: [], name: 'ME', type: 'scatter', mode: 'markers', showlegend: true,
      marker: {size: 12, color: '#808080'}};
    const MSTrace = {x: [], y: [], text: [], name: 'MS', type: 'scatter', mode: 'markers', showlegend: true,
      marker: {size: 12, color: '#FFA500'}};
    const HETrace = {x: [], y: [], text: [], name: 'HE', type: 'scatter', mode: 'markers', showlegend: true,
      marker: {size: 12, color: '#009E73'}};
    const HSTrace = {x: [], y: [], text: [], name: 'HS', type: 'scatter', mode: 'markers', showlegend: true,
      marker: {size: 12, color: '#56B4E9'}};

    const emptyPlot = {
      data: [],
      layout: {
        title: 'PCA Plot',
        showlegend: true
      }
    };

    if (this.contrast.match('Case-Control')) {
      emptyPlot.data.push(controlTrace, caseTrace);
    } else /*if (this.contrast.match('(ME-MS)-(HE-HS)'))*/{
      emptyPlot.data.push(METrace, MSTrace, HETrace, HSTrace);
    }
    const api = new ApiUtils('files');
    const aux = await api.getStudiesNames(this.user, this._selectedAnalysis);
    const rawData = await api.getPcaplotData('poiu', this.selectedAnalysis + '/pca' + (index + 1) + '.txt');
    const parsedData = JSON.parse(rawData);
    const test = parsedData.data;
    this.stdNames = aux.split('\n');

    for (let i = 0; i < parsedData.data.length; i++) {
      if (test[i].design.match('Case')) {
        caseTrace.x.push(test[i].PC1);
        caseTrace.y.push(test[i].PC2);
        caseTrace.text.push(test[i].sample);
      } else if (test[i].design.match('Control')){
        controlTrace.x.push(test[i].PC1);
        controlTrace.y.push(test[i].PC2);
        controlTrace.text.push(test[i].sample);
      } else if (test[i].design.match('ME')){
        METrace.x.push(test[i].PC1);
        METrace.y.push(test[i].PC2);
        METrace.text.push(test[i].sample);
      } else if (test[i].design.match('MS')){
        MSTrace.x.push(test[i].PC1);
        MSTrace.y.push(test[i].PC2);
        MSTrace.text.push(test[i].sample);
      } else if (test[i].design.match('HE')){
        HETrace.x.push(test[i].PC1);
        HETrace.y.push(test[i].PC2);
        HETrace.text.push(test[i].sample);
      } else if (test[i].design.match('HS')){
        HSTrace.x.push(test[i].PC1);
        HSTrace.y.push(test[i].PC2);
        HSTrace.text.push(test[i].sample);
      }
    }
    this.pcaValues.push(emptyPlot);
  }

  printPlots() {

    if (this.pcaValues[0]) {
      this.plot = this.pcaValues[0];
      this.plot.layout.title = this.stdNames[0].split('\t')[1];
      document.getElementById('pcaplot0').setAttribute('class', 'col s6');
    }
    if (this.pcaValues[1]) {
      this.plot1 = this.pcaValues[1];
      this.plot1.layout.title = this.stdNames[1].split('\t')[1];
      document.getElementById('pcaplot1').setAttribute('class', 'col s6 ');
    }
    if (this.pcaValues[2]) {
      this.plot2 = this.pcaValues[2];
      this.plot2.layout.title = this.stdNames[2].split('\t')[1];
      document.getElementById('pcaplot2').setAttribute('class', 'col s6');
    }
    if (this.pcaValues[3]) {
      this.plot3 = this.pcaValues[3];
      this.plot3.layout.title = this.stdNames[3].split('\t')[1];
      document.getElementById('pcaplot3').setAttribute('class', 'col s6 ');
    }
    if (this.pcaValues[4]) {
      this.plot4 = this.pcaValues[4];
      this.plot4.layout.title = this.stdNames[4].split('\t')[1];
      document.getElementById('pcaplot4').setAttribute('class', 'col s6');
    }
    if (this.pcaValues[5]) {
      this.plot5 = this.pcaValues[5];
      this.plot5.layout.title = this.stdNames[5].split('\t')[1];
      document.getElementById('pcaplot5').setAttribute('class', 'col s6 ');
    }
    if (this.pcaValues[6]) {
      this.plot6 = this.pcaValues[6];
      this.plot6.layout.title = this.stdNames[6].split('\t')[1];
      document.getElementById('pcaplot6').setAttribute('class', 'col s6');
    }
    if (this.pcaValues[7]) {
      this.plot7 = this.pcaValues[7];
      this.plot7.layout.title = this.stdNames[7].split('\t')[1];
      document.getElementById('pcaplot7').setAttribute('class', 'col s6 ');
    }
    if (this.pcaValues[8]) {
      this.plot8 = this.pcaValues[8];
      this.plot8.layout.title = this.stdNames[8].split('\t')[1];
      document.getElementById('pcaplot8').setAttribute('class', 'col s6');
    }
    if (this.pcaValues[9]) {
      this.plot9 = this.pcaValues[9];
      this.plot9.layout.title = this.stdNames[9].split('\t')[1];
      document.getElementById('pcaplot9').setAttribute('class', 'col s6 ');
    }
    if (this.pcaValues[10]) {
      this.plot10 = this.pcaValues[10];
      this.plot10.layout.title = this.stdNames[10].split('\t')[1];
      document.getElementById('pcaplot10').setAttribute('class', 'col s6');
    }
    if (this.pcaValues[11]) {
      this.plot11 = this.pcaValues[11];
      this.plot11.layout.title = this.stdNames[11].split('\t')[1];
      document.getElementById('pcaplot11').setAttribute('class', 'col s6 ');
    }
    if (this.pcaValues[12]) {
      this.plot12 = this.pcaValues[12];
      this.plot12.layout.title = this.stdNames[12].split('\t')[1];
      document.getElementById('pcaplot12').setAttribute('class', 'col s6');
    }
    if (this.pcaValues[13]) {
      this.plot13 = this.pcaValues[13];
      this.plot5.layout.title = this.stdNames[13].split('\t')[1];
      document.getElementById('pcaplot13').setAttribute('class', 'col s6 ');
    }
    if (this.pcaValues[14]) {
      this.plot14 = this.pcaValues[14];
      this.plot5.layout.title = this.stdNames[14].split('\t')[1];
      document.getElementById('pcaplot14').setAttribute('class', 'col s6');
    }
  }

  cleanData() {
    this.pcaValues = [];

    this.plot = {data: [],
      layout: {
        title: 'PCA Plot',
        showlegend: true
      }
    };
    this.plot1 = {data: [],
      layout: {
        title: 'PCA Plot',
        showlegend: true
      }
    };
    this.plot2 = {data: [],
      layout: {
        title: 'PCA Plot',
        showlegend: true
      }
    };
    this.plot3 = {data: [],
      layout: {
        title: 'PCA Plot',
        showlegend: true
      }
    };
    this.plot4 = {data: [],
      layout: {
        title: 'PCA Plot',
        showlegend: true
      }
    };
    this.plot5 = {data: [],
      layout: {
        title: 'PCA Plot',
        showlegend: true
      }
    };
    this.plot6 = {data: [],
      layout: {
        title: 'PCA Plot',
        showlegend: true
      }
    };
    this.plot7 = {data: [],
      layout: {
        title: 'PCA Plot',
        showlegend: true
      }
    };
    this.plot8 = {data: [],
      layout: {
        title: 'PCA Plot',
        showlegend: true
      }
    };
    this.plot9 = {data: [],
      layout: {
        title: 'PCA Plot',
        showlegend: true
      }
    };
    this.plot10 = {data: [],
      layout: {
        title: 'PCA Plot',
        showlegend: true
      }
    };
    this.plot11 = {data: [],
      layout: {
        title: 'PCA Plot',
        showlegend: true
      }
    };
    this.plot12 = {data: [],
      layout: {
        title: 'PCA Plot',
        showlegend: true
      }
    };
    this.plot13 = {data: [],
      layout: {
        title: 'PCA Plot',
        showlegend: true
      }
    };
    this.plot14 = {data: [],
      layout: {
        title: 'PCA Plot',
        showlegend: true
      }
    };
    for (let i = 0; i < 15; i++) {
      document.getElementById('scatter-plot' + i).setAttribute('class', 'hide col s6');
    }
  }
}
