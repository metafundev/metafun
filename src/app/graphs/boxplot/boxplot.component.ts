import {Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges} from '@angular/core';
import ApiUtils from '../../utils/ApiUtils';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-boxplot',
  templateUrl: './boxplot.component.html',
  styleUrls: ['./boxplot.component.css']
})
export class BoxplotComponent implements OnInit, OnChanges {

  private user;
  public debug = false;
  public useResizeHandler = true;
  private contrast = '';
  private bpValues = [];
  private stdNames = null;
  @Input() id: string;
  @Input() selectedAnalysis: string;

  public boxPlot = {
    data: [

    ],
    layout: {
      title: 'Box Plot'
    },
    config: {
      staticPlot: true,
      displayModeBar: false,
      responsive: true
    },
  };
  public boxPlot1 = {
    data: [

    ],
    layout: {
      title: 'Box Plot'
    },
    config: {
      staticPlot: true,
      displayModeBar: false,
      responsive: true
    },
  };
  public boxPlot2 = {
    data: [

    ],
    layout: {
      title: 'Box Plot'
    },
    config: {
      staticPlot: true,
      displayModeBar: false,
      responsive: true
    },
  };
  public boxPlot3 = {
    data: [

    ],
    layout: {
      title: 'Box Plot'
    },
    config: {
      staticPlot: true,
      displayModeBar: false,
      responsive: true
    },
  };
  public boxPlot4 = {
    data: [

    ],
    layout: {
      title: 'Box Plot'
    },
    config: {
      staticPlot: false,
      displayModeBar: true,
      responsive: true
    },
  };
  public boxPlot5 = {
    data: [

    ],
    layout: {
      title: 'Box Plot'
    },
    config: {
      staticPlot: true,
      displayModeBar: false,
      responsive: true
    },
  };
  public boxPlot6 = {
    data: [

    ],
    layout: {
      title: 'Box Plot'
    },
    config: {
      staticPlot: true,
      displayModeBar: false,
      responsive: true
    },
  };
  public boxPlot7 = {
    data: [

    ],
    layout: {
      title: 'Box Plot'
    },
    config: {
      staticPlot: true,
      displayModeBar: false,
      responsive: true
    },
  };
  public boxPlot8 = {
    data: [

    ],
    layout: {
      title: 'Box Plot'
    },
    config: {
      staticPlot: true,
      displayModeBar: false,
      responsive: true
    },
  };
  public boxPlot9 = {
    data: [

    ],
    layout: {
      title: 'Box Plot'
    },
    config: {
      staticPlot: true,
      displayModeBar: false,
      responsive: true
    },
  };
  public boxPlot10 = {
    data: [

    ],
    layout: {
      title: 'Box Plot'
    },
    config: {
      staticPlot: true,
      displayModeBar: false,
      responsive: true
    },
  };
  public boxPlot11 = {
    data: [

    ],
    layout: {
      title: 'Box Plot'
    },
    config: {
      staticPlot: true,
      displayModeBar: false,
      responsive: true
    },
  };
  public boxPlot12 = {
    data: [

    ],
    layout: {
      title: 'Box Plot'
    },
    config: {
      staticPlot: true,
      displayModeBar: false,
      responsive: true
    },
  };
  public boxPlot13 = {
    data: [

    ],
    layout: {
      title: 'Box Plot'
    },
    config: {
      staticPlot: true,
      displayModeBar: false,
      responsive: true
    },
  };
  public boxPlot14 = {
    data: [

    ],
    layout: {
      title: 'Box Plot'
    },
    config: {
      staticPlot: true,
      displayModeBar: false,
      responsive: true
    },
  };

  private _selectedAnalysis;
  constructor(
    private cookieService: CookieService,
    private router: Router
  ) {}

  ngOnInit() {
    this.user = this.cookieService.get('userLoged');
    // this.getBoxplotValues();
  }

  async ngOnChanges(changes: SimpleChanges) {
    const apiJ = new ApiUtils('jobs');
    const apiF = new ApiUtils('files');
    const selectedAnalysis: SimpleChange = changes.selectedAnalysis;
    this._selectedAnalysis = selectedAnalysis.currentValue;
    if (!(this._selectedAnalysis === undefined) || !(this._selectedAnalysis === 'undefined')) {
      this.contrast = await apiJ.getJobDesign(this.user, this._selectedAnalysis);
      this.cleanData();
      const emptyBoxPlot = {
        data: [],
        layout: {
          title: 'Box Plot'
        }
      };
      // Get Plot Count for analysis
      let count = 2;
      const sCount = await apiF.getPlotCount(this.user, this._selectedAnalysis);
      count = +sCount;
      for (let i = 0; i < count; i++) {
        await this.getBoxplotValues(i);
      }
      this.printPlots();
    }
  }


  async getBoxplotValues(bpID) {
    const bp = {
      data: [

      ],
      layout: {
        title: 'Box Plot'
      }
    };
    const api = new ApiUtils('files');
    const aux = await api.getStudiesNames(this.user, this._selectedAnalysis);
    this.stdNames = aux.split('\n');
    const leg1 = {x: [], y: [], name: 'Case', type: 'scatter', mode: 'lines+points', showlegend: true, hoverinfo: 'skip', marker: {color: '#808080'}};
    const leg2 = {x: [], y: [], name: 'Control', type: 'scatter', mode: 'lines+points', showlegend: true, hoverinfo: 'skip', marker: {color: '#FFA500'}};
    const legME = {x: [], y: [], name: 'ME', type: 'scatter', mode: 'lines+points', showlegend: true, hoverinfo: 'skip', marker: {color: '#808080'}};
    const legMS = {x: [], y: [], name: 'MS', type: 'scatter', mode: 'lines+points', showlegend: true, hoverinfo: 'skip', marker: {color: '#FFA500'}};
    const legHE = {x: [], y: [], name: 'HE', type: 'scatter', mode: 'lines+points', showlegend: true, hoverinfo: 'skip', marker: {color: '#009E73'}};
    const legHS = {x: [], y: [], name: 'HS', type: 'scatter', mode: 'lines+points', showlegend: true, hoverinfo: 'skip', marker: {color: '#56B4E9'}};

    const rawData = await api.getBoxplotData(this.user, this._selectedAnalysis + '/boxplot' + (bpID + 1)  + '.txt');
    const parsedData = JSON.parse(rawData);
    const test = parsedData.data;
    let bpDataValues;
    test.sort((a, b) => {
      return b.design < a.design ?  1
         : b.design > a.design ? -1
         : 0;
    });
    for (let i = 0; i < parsedData.data.length; i++) {
      bpDataValues = this.boxplotValues(test[i].values);
      let colorMarker: any;
      let legGroup: any;
      switch (test[i].design) {
        case 'Control':
          colorMarker = {color: '#FFA500'};
          legGroup = 'Control';
          break;
        case 'Case':
          colorMarker = {color: '#808080'};
          legGroup = 'Case';
          break;
        case 'ME':
          colorMarker = {color: '#808080'};
          legGroup = 'ME';
          break;
        case 'MS':
          colorMarker = {color: '#FFA500'};
          legGroup = 'MS';
          break;
        case 'HE':
          colorMarker = {color: '#009E73'};
          legGroup = 'HE';
          break;
        case 'HS':
          colorMarker = {color: '#56B4E9'};
          legGroup = 'HS';
          break;
      }
      bp.data.push({
        y: bpDataValues[0],
        outliers: bpDataValues[1],
        showlegend: false,
        name: test[i].sample,
        marker: colorMarker,
        text: test[i].design,
        legendgroup: legGroup,
        type: 'box'});
      if (test[i].design.match('Case')) {
        leg1.y.push(bpDataValues[0][3]);
        leg1.x.push(test[i].sample);
      } else if (test[i].design.match('Control')) {
        leg2.y.push(bpDataValues[0][3]);
        leg2.x.push(test[i].sample);
      } else if (test[i].design.match('ME')) {
        legME.y.push(bpDataValues[0][3]);
        legME.x.push(test[i].sample);
      } else if (test[i].design.match('MS')) {
        legMS.y.push(bpDataValues[0][3]);
        legMS.x.push(test[i].sample);
      } else if (test[i].design.match('HE')) {
        legHE.y.push(bpDataValues[0][3]);
        legHE.x.push(test[i].sample);
      } else if (test[i].design.match('HS')) {
        legHS.y.push(bpDataValues[0][3]);
        legHS.x.push(test[i].sample);
      }
    }
    if (this.contrast.match('Case-Control')) {
      bp.data.push(leg1, leg2);
    } else /*if (this.contrast.match('(ME-MS)-(HE-HS)'))*/{
      bp.data.push(legHE, legHS, legME, legMS);
    }
    this.bpValues.push(bp);

  }

  boxplotValues(jsonbpData) {
    const outliers = [];
    const Q1 = jsonbpData.Q1;
    const Q2 = jsonbpData.Q2;
    const Q3 = jsonbpData.Q3;
    let wMin = +Q1 - (1.5 * (+Q3 - +Q1));
    let wMax = +Q3 + (1.5 * (+Q3 - +Q1));
    if (wMin > jsonbpData.min) {
      outliers.push(jsonbpData.min);
    } else {
      wMin = jsonbpData.min;
    }

    if (wMax < jsonbpData.max) {
      outliers.push(jsonbpData.max);
    } else {
      wMax = jsonbpData.max;
    }
    const yData = [wMin, Q1, Q1, Q2, Q2, Q3, Q3, wMax];

    return [yData, outliers];
  }

  printPlots() {
    if (this.bpValues[0]) {
      this.boxPlot = this.bpValues[0];
      this.boxPlot.layout.title = this.stdNames[0].split('\t')[1];
      document.getElementById('boxplot0').setAttribute('class', 'col s6');
    }
    if (this.bpValues[1]) {
      this.boxPlot1 = this.bpValues[1];
      this.boxPlot1.layout.title = this.stdNames[1].split('\t')[1];

      document.getElementById('boxplot1').setAttribute('class', 'col s6 ');
    }
    if (this.bpValues[2]) {
      this.boxPlot2 = this.bpValues[2];
      this.boxPlot2.layout.title = this.stdNames[2].split('\t')[1];

      document.getElementById('boxplot2').setAttribute('class', 'col s6');
    }
    if (this.bpValues[3]) {
      this.boxPlot3 = this.bpValues[3];
      this.boxPlot3.layout.title = this.stdNames[3].split('\t')[1];

      document.getElementById('boxplot3').setAttribute('class', 'col s6 ');
    }
    if (this.bpValues[4]) {
      this.boxPlot4 = this.bpValues[4];
      this.boxPlot4.layout.title = this.stdNames[4].split('\t')[1];
      document.getElementById('boxplot4').setAttribute('class', 'col s6');
    }
    if (this.bpValues[5]) {
      this.boxPlot5 = this.bpValues[5];
      this.boxPlot5.layout.title = this.stdNames[5].split('\t')[1];
      document.getElementById('boxplot5').setAttribute('class', 'col s6 ');
    }
    if (this.bpValues[6]) {
      this.boxPlot6 = this.bpValues[6];
      this.boxPlot6.layout.title = this.stdNames[6].split('\t')[1];
      document.getElementById('boxplot6').setAttribute('class', 'col s6');
    }
    if (this.bpValues[7]) {
      this.boxPlot7 = this.bpValues[7];
      this.boxPlot7.layout.title = this.stdNames[7].split('\t')[1];
      document.getElementById('boxplot7').setAttribute('class', 'col s6 ');
    }
    if (this.bpValues[8]) {
      this.boxPlot8 = this.bpValues[8];
      this.boxPlot8.layout.title = this.stdNames[8].split('\t')[1];
      document.getElementById('boxplot8').setAttribute('class', 'col s6');
    }
    if (this.bpValues[9]) {
      this.boxPlot9 = this.bpValues[9];
      this.boxPlot9.layout.title = this.stdNames[9].split('\t')[1];
      document.getElementById('boxplot9').setAttribute('class', 'col s6 ');
    }
    if (this.bpValues[10]) {
      this.boxPlot10 = this.bpValues[10];
      this.boxPlot10.layout.title = this.stdNames[10].split('\t')[1];
      document.getElementById('boxplot10').setAttribute('class', 'col s6');
    }
    if (this.bpValues[11]) {
      this.boxPlot11 = this.bpValues[11];
      this.boxPlot11.layout.title = this.stdNames[11].split('\t')[1];
      document.getElementById('boxplot11').setAttribute('class', 'col s6 ');
    }
    if (this.bpValues[12]) {
      this.boxPlot12 = this.bpValues[12];
      this.boxPlot12.layout.title = this.stdNames[12].split('\t')[1];
      document.getElementById('boxplot12').setAttribute('class', 'col s6');
    }
    if (this.bpValues[13]) {
      this.boxPlot13 = this.bpValues[13];
      this.boxPlot13.layout.title = this.stdNames[13].split('\t')[1];
      document.getElementById('boxplot13').setAttribute('class', 'col s6 ');
    }
    if (this.bpValues[14]) {
      this.boxPlot14 = this.bpValues[14];
      this.boxPlot14.layout.title = this.stdNames[14].split('\t')[1];
      document.getElementById('boxplot14').setAttribute('class', 'col s6');
    }
  }

  cleanData() {
    this.bpValues = [];
    this.boxPlot = {
      data: [

      ],
      layout: {
        title: 'Box Plot'
      },
      config: {
        staticPlot: false,
        displayModeBar: true,
        responsive: true
      },
    };
    this.boxPlot1 = {
      data: [

      ],
      layout: {
        title: 'Box Plot'
      },
      config: {
        staticPlot: false,
        displayModeBar: true,
        responsive: true
      },
    };
    this.boxPlot2 = {
      data: [

      ],
      layout: {
        title: 'Box Plot'
      },
      config: {
        staticPlot: false,
        displayModeBar: true,
        responsive: true
      },
    };
    this.boxPlot3 = {
      data: [

      ],
      layout: {
        title: 'Box Plot'
      },
      config: {
        staticPlot: false,
        displayModeBar: true,
        responsive: true
      },
    };
    this.boxPlot4 = {
      data: [

      ],
      layout: {
        title: 'Box Plot'
      },
      config: {
        staticPlot: false,
        displayModeBar: true,
        responsive: true
      },
    };
    this.boxPlot5 = {
      data: [

      ],
      layout: {
        title: 'Box Plot'
      },
      config: {
        staticPlot: false,
        displayModeBar: true,
        responsive: true
      },
    };
    this.boxPlot6 = {
      data: [

      ],
      layout: {
        title: 'Box Plot'
      },
      config: {
        staticPlot: false,
        displayModeBar: true,
        responsive: true
      },
    };
    this.boxPlot7 = {
      data: [

      ],
      layout: {
        title: 'Box Plot'
      },
      config: {
        staticPlot: false,
        displayModeBar: true,
        responsive: true
      },
    };
    this.boxPlot8 = {
      data: [

      ],
      layout: {
        title: 'Box Plot'
      },
      config: {
        staticPlot: false,
        displayModeBar: true,
        responsive: true
      },
    };
    this.boxPlot9 = {
      data: [

      ],
      layout: {
        title: 'Box Plot'
      },
      config: {
        staticPlot: false,
        displayModeBar: true,
        responsive: true
      },
    };
    this.boxPlot10 = {
      data: [

      ],
      layout: {
        title: 'Box Plot'
      },
      config: {
        staticPlot: false,
        displayModeBar: true,
        responsive: true
      },
    };
    this.boxPlot11 = {
      data: [

      ],
      layout: {
        title: 'Box Plot'
      },
      config: {
        staticPlot: false,
        displayModeBar: true,
        responsive: true
      },
    };
    this.boxPlot12 = {
      data: [

      ],
      layout: {
        title: 'Box Plot'
      },
      config: {
        staticPlot: false,
        displayModeBar: true,
        responsive: true
      },
    };
    this.boxPlot13 = {
      data: [

      ],
      layout: {
        title: 'Box Plot'
      },
      config: {
        staticPlot: false,
        displayModeBar: true,
        responsive: true
      },
    };
    this.boxPlot14 = {
      data: [

      ],
      layout: {
        title: 'Box Plot'
      },
      config: {
        staticPlot: false,
        displayModeBar: true,
        responsive: true
      },
    };
    for (let i = 0; i < 15; i++) {
      document.getElementById('boxplot' + i).setAttribute('class', 'hide');
    }
  }
}

