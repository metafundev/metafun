import {AfterViewInit, Component, OnInit} from '@angular/core';
import {NgxSmartModalService} from 'ngx-smart-modal';

@Component({
  selector: 'app-file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.css']
})
export class FileUploaderComponent implements OnInit, AfterViewInit {
  private basePath;
  private user: string;
  private files;
  private backFolder;
  private selectedFile;
  private currentFolder: string;
  private pathBar;
  constructor() { }

  ngOnInit() {
    console.log ('onInitModal');
    const exprMatInput = document.getElementById('realCSVInput');
    console.log(exprMatInput);
    const csvInfo = document.getElementById('csvInfoA');
    exprMatInput.addEventListener('change', () => {
      // @ts-ignore
      const name = exprMatInput.value.split(/\\|\//).pop();
      const truncated = name.length > 25
        ? name.substr(name.length - 25)
        : name;

      csvInfo.innerHTML = truncated;
    });
    const ddfMatInput = document.getElementById('realDDFInput');
    const ddfInfo = document.getElementById('ddfInfoA');
    ddfMatInput.addEventListener('change', () => {
      // @ts-ignore
      const name = ddfMatInput.value.split(/\\|\//).pop();
      const truncated = name.length > 25
        ? name.substr(name.length - 25)
        : name;
      console.log('ñlkadjf');
      ddfInfo.innerHTML = truncated;
    });
  }

  ngAfterViewInit() {
    const exprMatInput = document.getElementById('realCSVInput');
    const csvInfo = document.getElementById('csvInfoA');
    exprMatInput.addEventListener('change', () => {
      // @ts-ignore
      const name = exprMatInput.value.split(/\\|\//).pop();
      const truncated = name.length > 25
        ? name.substr(name.length - 25)
        : name;

      csvInfo.innerHTML = truncated;
    });
    const ddfMatInput = document.getElementById('realDDFInput');
    const ddfInfo = document.getElementById('ddfInfoA');
    ddfMatInput.addEventListener('change', () => {
      // @ts-ignore
      const name = ddfMatInput.value.split(/\\|\//).pop();
      const truncated = name.length > 25
        ? name.substr(name.length - 25)
        : name;
      console.log('ñlkadjf');
      ddfInfo.innerHTML = truncated;
    });
  }

  ddInfoChange () {
    console.log ('click');
  }
}
