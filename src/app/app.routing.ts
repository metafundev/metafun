import {HomeComponent} from './home/home.component';
import {RouterModule} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';
import {NewJobComponent} from './new-job/new-job.component';
import {UserprofileComponent} from './userprofile/userprofile.component';
import {JobsPageComponent} from './jobs-page/jobs-page.component';
import {DataPageComponent} from './data-page/data-page.component';


const appRoutes = [

  { path: '', component: HomeComponent, data: {page: '1'}},
  { path: 'login', component: LoginComponent, data: {page: '2'}},
  { path: 'signup', component: SignupComponent, data: {page: '3'}},
  { path: 'newjob', component: NewJobComponent, data: {page: '4'}},
  { path: 'home', component: HomeComponent, data: {page: '5'}},
  { path: 'userprofile', component: UserprofileComponent, data: {page: '6'}},
  { path: 'jobspage', component: JobsPageComponent, data: {page: '7'}},
  { path: '**', component: HomeComponent, data: {page: '10'}}

];

export const APP_ROUTES = RouterModule.forRoot( appRoutes );
