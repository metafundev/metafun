import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import ApiUtils from '../utils/ApiUtils';
import * as CryptoJS from 'crypto-js';
import {Router} from '@angular/router';
import {MenuComponent} from '../menu/menu.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public formLogIn: FormGroup;
  public mostrarError = false;
  public mensajeError: string [] = [];
  private cookieValue = 'NAN';
  constructor(private cookieService: CookieService, private router: Router) {

    this.formLogIn = new FormGroup(
      {
        eMail: new FormControl('', [Validators.required, Validators.minLength(3)]),
        pass: new FormControl('', [Validators.required, Validators.minLength(3)])
      }
    );
  }

  ngOnInit() {
    if (this.cookieService.check('userLoged')) {
      this.router.navigate(['userprofile']);
    }
  }

  async onSubmit() {
    const api = new ApiUtils('users');
    const mail = this.formLogIn.controls.eMail.value;
    const pass = CryptoJS.MD5(this.formLogIn.controls.pass.value);
    const res = await api.logInRequest(mail,  pass);
    let name = null;
    // @ts-ignore
    if (res === true) {
      name = await api.getUserName(mail);
      this.cookieService.set('userLoged', name);
      MenuComponent.reloadSignUpText('');
      MenuComponent.reloadLogIntext(name);
      this.ngOnInit();
    } else {
      alert('Mail or password incorrect');
      this.limpiaForm();
    }
  }

  limpiaForm() {
    this.mensajeError = [];
    this.mostrarError = false;
    this.formLogIn.get('eMail').setValue('');
    this.formLogIn.get('pass').setValue('');
  }

}
