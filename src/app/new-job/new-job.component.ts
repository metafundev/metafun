import { Component, OnInit } from '@angular/core';
import ApiUtils from '../utils/ApiUtils';
import {CookieService} from 'ngx-cookie-service';
import {CdkDrag, CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {Router} from '@angular/router';


@Component({
  selector: 'app-new-job',
  templateUrl: './new-job.component.html',
  styleUrls: ['./new-job.component.css']
})
export class NewJobComponent implements OnInit {

  private user = undefined;
  allFolders;
  private allFiles = [];
  public allNamesFiles = [];
  public csvNamesFiles = [];
  public ddfNamesFiles = [];

  constructor(private cookieService: CookieService, private router: Router) {
  }

  async ngOnInit() {
    this.user = this.cookieService.get('userLoged');
    const api = new ApiUtils('files');
    const fol = [];
    if (this.user !== '') {
      this.allFiles = JSON.parse(await api.getFilesFromUser(this.user));
      const compareCombo = document.getElementById('compareOptionsCombo');
      const launchButton = document.getElementById('launchButton');
      launchButton.addEventListener('click', (e: Event) => this.getLaunchOptions());
      compareCombo.addEventListener('change', (e: Event) => this.comboChanged());
      this.allFolders = await api.getUserFolders(this.user);
      this.allFolders = JSON.parse(this.allFolders);
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0 ; i < this.allFiles.length; i++) {
        if (this.allFiles[i].type.match('FOLDER')) {
          fol.push(this.allFiles[i].name);
        }
      }
      console.log (this.allFolders);
      let fileVal = '';
      let iconVal = '';
      for (let i = 0 ; i < fol.length; i ++) {
        this.allNamesFiles.push({value: fol[i], icon: 'folder_open', disabled: true, file: null});
        for (let j = 0 ; j < this.allFolders[fol[i]].length ; j++) {
          fileVal = this.allFolders[fol[i]][j];
          // @ts-ignore
          fileVal.substring(fileVal.length - 3, fileVal.length).match('csv') ? iconVal = 'assessment' : iconVal = 'assignment';
          // @ts-ignore
          this.allNamesFiles.push({value: fileVal, icon: iconVal, file: null});
        }
      }
      console.log ('folders --> ', this.allNamesFiles.length, 'folders -->', this.allFiles.length);
      console.log (this.allFiles);
      console.log (this.allNamesFiles);
      for (let i = 0; i < this.allFiles.length; i++) {
        for (let j = 0; j < this.allFiles.length; j++) {
          if (this.allFiles[i].name.match(this.allNamesFiles[j].value)) {
            this.allNamesFiles[j].file = this.allFiles[i];
          }
        }
      }
    } else {
      this.router.navigate(['/']);
    }
    console.log(this.allNamesFiles);
  }

  drop(event: CdkDragDrop<string[]>) {
    document.getElementById('compareOptionsCombo').addEventListener('change', (e: Event) => this.comboChanged());
    if (event.previousContainer === event.container) {
      if (!event.previousContainer.element.nativeElement.getAttribute('id').match('allList')) {
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      }
      if ((this.ddfNamesFiles.length === this.csvNamesFiles.length) && (this.ddfNamesFiles.length > 2)) {
        this.checkExperimentalDesign();
      }
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      // @ts-ignore
      if ((this.ddfNamesFiles.length === this.csvNamesFiles.length) && (this.ddfNamesFiles.length >= 2)) {
        this.checkExperimentalDesign();
      }
    }
  }

  async checkExperimentalDesign() {
    const api = new ApiUtils('files');
    const experimentalDiv = document.getElementById('experimentalDiv');
    const responses = [];
    for (let i = 0; i < this.ddfNamesFiles.length; i++) {
      const resp = await api.checkExperimentalDesign(this.user, this.csvNamesFiles[i].file.path, this.ddfNamesFiles[i].file.path);
      responses.push(resp);
    }
    let flag = true;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < responses.length; i++) {
      if (responses[i].match('false')) {
        flag = false;
      }
    }
    if (flag === true) {
      this.fillExperimentalDesignTable();
    } else {
      experimentalDiv.innerHTML = '<div  class="col s12 center metaBackground">' +
        '<br><br><h4  class="metaTextAccent">Data Options</h4>' +
        '<div class="blue-grey"><a  style="color: white"><h6 >Compare Options</h6></a></div>' +
        '<div class="col s12">' +
        '<select class="browser-default" id="compareOptionsCombo">' +
        '<option >Case - Control</option><option >(Case Female - Control Female) - (Case Male - Control Male)</option></select>' +
        '<div><table  id="compareOptionsTable">' +
        '<thead id="optionsTableHead"><th > CSV File </th><th > Case </th><th > Control </th></thead>' +
        '<tbody id="compareOptionsTableBody"></tbody></table></div></div></div>';
    }
  }

  async comboChanged() {
    const compareCombo = document.getElementById('compareOptionsCombo') as HTMLOptionElement;
    const optTHead = document.getElementById('optionsTableHead');
    const api = new ApiUtils('files');
    const tableBody = document.getElementById('compareOptionsTableBody');
    console.log('Combo changed');
    tableBody.innerHTML = '';
    let tags = '';
    let k = 0;
    if (compareCombo.value.match('CS')) {
      optTHead.innerHTML = '<th class="center">\n' +
        '                CSV File\n' +
        '              </th>\n' +
        '              <th class="center">\n' +
        '                Case\n' +
        '              </th>\n' +
        '              <th class="center">\n' +
        '                Control\n' +
        '              </th>';

      for (let i = 0; i < this.ddfNamesFiles.length; i++) {
        tags = await api.getExperimentalDesign(this.user, this.ddfNamesFiles[i].file.path.substring((31 + this.user.length)));
        const tagsR = tags.split(';');
        let sel = '<select class="browser-default" name="edSelect" id="edSelect_' + k + '">';
        let sel2  = '<select class="browser-default" name="edSelect" id="edSelect_' + (k + 1) + '">';
        for (let j = 0; j < tagsR.length - 1; j++) {
          sel += '<option>' + tagsR[j] + '</option>';
          sel2 += '<option>' + tagsR[j] + '</option>';
        }
        sel += '</select>';
        sel2 += '</select>';
        tableBody.innerHTML +=
          '<tr>' +
          ' <td>' + this.csvNamesFiles[i].file.name + '</td>' +
          ' <td>' + sel + '</td>' + '<td>' + sel2 + '</td>' +
          '</tr>';
        k += 2;
      }

    } else {
      optTHead.innerHTML = '<th>\n' +
        '                CSV File\n' +
        '              </th>\n' +
        '              <th class="center">\n' +
        '                Case Female\n' +
        '              </th>\n' +
        '              <th class="center">\n' +
        '                Control Female\n' +
        '              </th>' +
        '                <th class="center">\n' +
        '                  Case Male\n' +
        '                </th>\n' +
        '                <th class="center">\n' +
        '                  Control Male\n' +
        '                </th>';
      console.log ('¿?¿?¿?¿?¿??¿?¿---->', tags, this.ddfNamesFiles);
      for (let i = 0; i < this.ddfNamesFiles.length; i++) {
        tags = await api.getExperimentalDesign(this.user, this.ddfNamesFiles[i].file.parentDir + this.ddfNamesFiles[i].value);
        const tagsR = tags.split(';');
        let sel = '<select class="browser-default" name="edSelect" id="edSelect_' + k + '">';
        let sel2 = '<select class="browser-default" name="edSelect" id="edSelect_' + (k + 1) + '">';
        let sel3 = '<select class="browser-default" name="edSelect" id="edSelect_' + (k + 2) + '">';
        let sel4 = '<select class="browser-default" name="edSelect" id="edSelect_' + (k + 3) + '">';
        for (let j = 0; j < tagsR.length - 1; j++) {
          sel += '<option>' + tagsR[j] + '</option>';
          sel2 += '<option>' + tagsR[j] + '</option>';
          sel3 += '<option>' + tagsR[j] + '</option>';
          sel4 += '<option>' + tagsR[j] + '</option>';
        }
        sel += '</select>';
        sel2 += '</select>';
        sel3 += '</select>';
        sel4 += '</select>';
        tableBody.innerHTML +=
          '<tr>' +
          ' <td>' + this.csvNamesFiles[i].file.name + '</td>' +
          ' <td>' + sel + '</td>' + '<td>' + sel2 + '</td>' + ' <td>' + sel3 + '</td>' + '<td>' + sel4 + '</td>' +
          '</tr>';
        k += 4;
      }
    }
  }

  async fillExperimentalDesignTable() {
    const api = new ApiUtils('files');
    const tableBody = document.getElementById('compareOptionsTableBody');
    tableBody.innerHTML = '';
    let tags = '';
    let k = 0;
    for (let i = 0; i < this.ddfNamesFiles.length; i++) {
      tags = await api.getExperimentalDesign(this.user, this.ddfNamesFiles[i].file.path.substring((31 + this.user.length)));
      const tagsR = tags.split(';');
      let sel = '<select class="browser-default" name="edSelect" id="edSelect_' + k + '">';
      let sel2 = '<select class="browser-default" name="edSelect" id="edSelect_' + (k + 1) + '">';
      for (let j = 0; j < tagsR.length - 1; j++) {
        sel += '<option>' + tagsR[j] + '</option>';
        sel2 += '<option>' + tagsR[j] + '</option>';
      }
      sel += '</select>';
      sel2 += '</select>';
      tableBody.innerHTML +=
        '<tr>' +
        ' <td>' + this.csvNamesFiles[i].file.name + '</td>' +
        ' <td>' + sel + '</td>' + '<td>' + sel2 + '</td>' +
        '</tr>';
      k += 2;
    }
  }

  getLaunchOptions() {
    const metaMethod: NodeList = document.getElementsByName('radioMethod');
    const funcChar: NodeList = document.getElementsByName('radioFunctional');
    const reference: NodeList = document.getElementsByName('radioReference');
    const jobNameInput = document.getElementById('jobNameInput') as HTMLInputElement;
    const designAssociation = [];
    const selectedDesign = document.getElementById('compareOptionsCombo') as HTMLOptionElement;
    let design = 0;
    const designValues = [];
    const ops = [];
    const checked = [];
    const jobName =  jobNameInput.value.replace(new RegExp(' ', 'g'), '_');


    for (let i = 0; i < metaMethod.length; i++) {
      // @ts-ignore
      if (metaMethod[i].checked) {
        ops.push(i);
      }
    }
    for (let i = 0; i < funcChar.length; i++) {
      // @ts-ignore
      if (funcChar[i].checked) {
        ops.push(i);
      }
    }
    for (let i = 0; i < reference.length; i++) {
      // @ts-ignore
      if (reference[i].checked) {
        ops.push(i);
      }
    }
    let k = 0;
    if (selectedDesign.value.match('CS')) {
      k = 2;
      ops.push(0);
    } else {
      k = 4;
      ops.push(1);
    }

    for (let i = 0; i < this.ddfNamesFiles.length * k; i++) {
      designAssociation.push(document.getElementById('edSelect_' + i) as HTMLOptionElement);
    }
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < designAssociation.length; i++) {
      if (designAssociation[i] != null) {
        designValues.push(designAssociation[i].value);
      }
    }
    this.launchJob(jobName, this.csvNamesFiles, this.ddfNamesFiles, designValues, ops);
  }


  launchJob(jobName, exprMatFiles, expDesignFiles, designValues, optionsArray) {
    const api = new ApiUtils('jobs');
    const edFiles = [];
    for (let i = 0 ; i < expDesignFiles.length ; i++) {
      edFiles.push(expDesignFiles[i].file);
    }

    const exprTech = [];
    let designString = '';

    let commandArgs = '';
    if (optionsArray[0] === 1) {
      commandArgs += '-FixedEffect';
    } else {
      commandArgs += '-RandomEffect';
    }

    if (optionsArray[1] === 1) {
      commandArgs += '-GSA'; // ' -Hipathia';
    } else {
      commandArgs += ' -GSA';
    }

    if (optionsArray[2] === 0) {
      commandArgs += ' -HS';
    } else if (optionsArray[2] === 1) {
      commandArgs += ' -MM';
    } else if (optionsArray[2] === 2) {
      commandArgs += ' -RN';
    }

    if (optionsArray[3] === 0) {
      commandArgs += ' -CS';
    } else {
      commandArgs += ' -GENDER';
    }

    for (let i = 0 ; i < designValues.length ; i++) {
      designString += designValues[i] + '@';
    }

    for (let i = 0; i < exprMatFiles.length; i++) {
      exprTech.push(exprMatFiles[i].file.path + '#' + exprMatFiles[i].file.tech);
    }

    api.registerJob(this.user, jobName, exprTech, edFiles, designString, commandArgs);
    alert('Job Launched Correctly');
    this.router.navigate(['userprofile']);
  }

  /** Predicate function that only allows even numbers to be dropped into a list. */
  isCSVPredicate(item: CdkDrag<string>) {
    // @ts-ignore
    if (item.data.icon.match('assessment')) {
      return true;
    }
    return false;
  }

  isDDFPredicate(item: CdkDrag<string>) {
    // @ts-ignore
    if (item.data.icon.match('assignment')) {
      return true;
    }
    return false;
  }


  /** Predicate function that doesn't allow items to be dropped into a list. */
  noReturnPredicate() {
    return true;
  }
}
