import {Component, Input, OnInit} from '@angular/core';
import ApiUtils from '../utils/ApiUtils';
import {CookieService} from 'ngx-cookie-service';
import {Router, RouterModule} from '@angular/router';
import {MenuComponent} from '../menu/menu.component';
import {getClassName} from 'codelyzer/util/utils';
import {NgxSmartModalService} from 'ngx-smart-modal';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css']
})
export class UserprofileComponent implements OnInit {
  private _http: any;
  private message: any;
  constructor(
    public ngxSmartModalService: NgxSmartModalService,
    private cookieService: CookieService,
    private router: Router
  ) {}
  jobs;
  diskusage;
  studies = [];
  user;

  Name: string;
  myFile: File; /* property of File type */

  @Input() selectedFile;


  fileChange(files: any) {
    const inFile = document.getElementById('myFile') as HTMLInputElement;
    this.myFile = inFile.files[0];
  }


  async ngOnInit() {
    const apiJobs = new ApiUtils('jobs');
    const apiFiles = new ApiUtils('files');
    let auxjobs;
    this.user = this.cookieService.get('userLoged');
    if (this.user !== '') {
      const rawJobs = await apiJobs.getJobsFromUser(this.user);
      let cadena = [];
      const files = [];
      auxjobs = JSON.parse(rawJobs);
      for (let i = 0; i < auxjobs.length ; i++) {
        cadena = auxjobs[i].exprFiles.split('@');
        cadena.pop();
        files.push(cadena);
      }
      auxjobs.sort((a , b) => {
        return new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime();
      });
      this.jobs = auxjobs.reverse();
      console.log ('AUX', auxjobs, 'this', this.jobs);
      const items = [];
      for (let i = 0; i < files.length; i++) {
        items[i] = [];
        for (let j = 0; j < files[i].length; j++) {
          items[i].push(files[i][j].split('#')[0].split('/')[files[i][j].split('#')[0].split('/').length - 1]);
        }
      }
      this.studies = items.reverse();
      const dus = await apiFiles.getDiskUsage(this.user);
      this.diskusage = 'You have uploaded ' + dus + ' in files';
      for (let i = 0 ; i < this.jobs.length ; i++) {
        const text = this.studies[i].toString();
        document.getElementById('studies' + i).innerText = text.replace(new RegExp(',', 'g'), '\n');
      }
    } else {
      this.router.navigate(['/']);
    }
  }

  async deleteJob(index: number) {
    const api = new ApiUtils('jobs');
    let txt;
    if (confirm('You are about to delete the next Job and all the results of it: \n\n\t- ' + this.jobs[index].name + '\n\nAre you sure?')) {
      api.deleteJob(this.user, this.jobs[index].name);
      this.ngOnInit();
      txt = 'You pressed OK!';
    } else {
      txt = 'You pressed Cancel!';
    }
  }

  showJobsInfo() {
    document.getElementById('jobsInfoUserDiv').setAttribute('class', '');
    document.getElementById('fileBrowserUserDiv').setAttribute('class', 'hide');
    document.getElementById('filesli').setAttribute('class', '');
    document.getElementById('jobsli').setAttribute('class', 'active');
  }

  showfilesInfo() {
    document.getElementById('jobsInfoUserDiv').setAttribute('class', 'hide');
    document.getElementById('fileBrowserUserDiv').setAttribute('class', 'col s10 push-s2');
    document.getElementById('filesli').setAttribute('class', 'active');
    document.getElementById('jobsli').setAttribute('class', '');
  }

  logOut() {
    this.cookieService.delete('userLoged');
    this.router.navigate(['/']);
    MenuComponent.reloadLogIntext('LogIn');
    MenuComponent.reloadSignUpText('SignUp');
  }

  onSubmit(): void {
    const api = new ApiUtils('files');
    const a = document.getElementById('myFile') as HTMLInputElement;
    api.nguploadFile(a.files[0], this.user, a.files[0].name, 'FILE', 'NA', 'currentFolder', 'currentFolder');
  }
}
